/**
 * SIGN PAGE
 */
const login_btn             = document.querySelector('.login-btn');
const sing_up_btn           = document.querySelector('.sign-up-btn');
const connexion_left_side   = document.querySelector('.sign-in-left');
const connexion_right_side  = document.querySelector('.sign-in-right');
const inscription_left_side = document.querySelector('.sign-up-left');
const inscription_right_side= document.querySelector('.sign-up-right');
// const form_group = $('.form-group div');

const humburger = document.querySelector('#humburger_js');
let humburger_container = document.querySelector('#humburger_container_js');

humburger.addEventListener('click',e=>{
    if (humburger_container) {
        humburger_container.classList.toggle("active");
    }
});

let switch_to_login_js  = document.querySelector("#switch_to_login_js");
let switch_to_sign_up_js= document.querySelector("#switch_to_sign_up_js");

if (switch_to_sign_up_js){
    switch_to_sign_up_js.addEventListener('click',e=>{
        e.preventDefault();
        let connection_container = document.querySelector('.sign-inner');
        if (connection_container) {
            connection_container.classList.add('active');
        }
    });
}
if (switch_to_login_js){
    switch_to_login_js.addEventListener('click',e=>{
        e.preventDefault();
        let connection_container = document.querySelector('.sign-inner');
        if (connection_container) {
            connection_container.classList.remove('active');
        }
    });
}

messageContact('#send_contact_js');

setEvent();

/**
 * It set events form login and sign up boutons
 */
function setEvent() {

    if (login_btn && sing_up_btn) {

        login_btn.addEventListener('click', e => {
            e.preventDefault();
            animate(e, false);
        });
        sing_up_btn.addEventListener('click', e => {
            e.preventDefault();
            animate(e, true);
        });
    }
}

/**
 *
 * @param {*} Event
 * @param {*} action add/remove
 */
function animate(Event, action) {
    if (Event.currentTarget.parentElement && action) {
        //signup
        Event.currentTarget.parentElement.classList.add('move-to-left');
        inscription_left_side.classList.add("move-to-right");

        //signin
        connexion_right_side.classList.add("move-to-left");
        connexion_left_side.classList.add("move-to-right");
    } else {
        //signin
        Event.currentTarget.parentElement.classList.remove('move-to-left');
        connexion_left_side.classList.remove("move-to-right");

        //signup
        inscription_left_side.classList.remove('move-to-right');
        inscription_right_side.classList.remove('move-to-left');
    }
}

//SEARCH - INPUT FIELD
var $users          = null;

searchInputField("#nav_bar_input_js");

function searchInputField(element_p) {
    let element_v = $(element_p);
    let container_calendar = $('.searchResultat');

    //Limite request number
    if (element_v) {
        //EVENT ON MOUSE DOWN ON SEARCH INPUT FIELD ON NAVBAR
        element_v.mousedown(e=>{

            let url         = "";
            let url_temp    = e.currentTarget.dataset.url.split("/") ;
            let data        = {name:'search'};
            container_calendar.toggleClass('show');

            //traite url
            if (!isNaN(parseInt(url_temp[url_temp.length-1]))) {
                //remove number on the last index
                url_temp.pop();
                url = url_temp.join('/');
            }else {
                url = e.currentTarget.dataset.url;
            }

            if (!$users) {
                getUsers(url+"/calendar/search",data, container_calendar);
            }

        });

        //EVENT ON KEY UP
        element_v.keyup(e=>{
            let container_ul_li = document.querySelectorAll('.main-header-nav-search-result-calendar');
            search(container_ul_li,element_v.val());
        })
    }
}

function searchMembers(element_p) {
    let element_v = $(element_p);
    //Limite request number
    if (element_v) {

        //EVENT ON MOUSE DOWN ON SEARCH INPUT FIELD ON NAVBAR
        element_v.mousedown( e => {
            console.log(element_v)

            if (!$users) {
                url = "/users/dashboard/calendar/search";
                $.ajax({
                    url: url,
                    type:"GET",
                    data: JSON.stringify({name:"members"}),
                    cache: false,
                    success: function (response) {
                        $users          = JSON.parse(response);
                        $users.forEach($user => {
                            let option  = document.createElement('option');
                            option.classList.add('members-option');
                            option.attr('value',$user.id);
                            option.innerHTML= `${$user.name} ${$user.lastname}`;
                            element_v.appendChild(option);
                        });
                    },
                    error: function (error) {
                        console.error(error);
                    }
                });
            }

        });

        //EVENT ON KEY UP
        element_v.keyup(e=>{
            let container_ul_li = document.querySelectorAll('.main-header-nav-search-result-calendar');
            search(container_ul_li,element_v.val());
        })
    }
}

/**
 * Ajax request, when user click on input element
 * this function sends ajax request to serveur in the order to get all users as members
 * @param url
 * @param data
 * @param container_element
 */
function getUsers(url, data,container_element) {
    $.ajax({
        url: url,
        type:"POST",
        data: JSON.stringify(data),
        cache: false,
        success: function (response) {
            $users          = JSON.parse(response);
            let ul_element  = document.createElement('ul');
            $users.forEach($user =>{
                createCalendarElementOnInput(ul_element,$user);
            })
            container_element.append(ul_element);
        },
        error: function (error) {
            console.error(error)
        }
    });
}

/**
 * Creates list of users on the input element
 * @param container_ul
 * @param user
 */
function createCalendarElementOnInput(container_ul, user) {
    let li = document.createElement('li');
    li.classList.add('main-header-nav-search-result-calendar')
    li.innerHTML = `
        <div>
            <img src="/build/images/${user.picture?user.picture:'profile_bg.2cd177cd.jpg'}"  alt="${user.name}'s picture">
        </div>
        <a href="/users/calendar/${user.id}">${user.name} ${user.lastname}</a>
   `;

   container_ul.appendChild(li);
}

/**
 * fonction used when user search calendar/user
 * @param container
 * @param text
 */
function search(container, text) {

    container.forEach(element => {
        if (element.textContent.toLowerCase().indexOf(text.toLowerCase()) !== -1) {
            element.style.display = "flex";
        }
        else {
           element.style.display = "none";
        }
    })
}


function messageContact(element_p) {
    let element_v = $(element_p);
    if (element_v && element_v.length) {
        element_v.click(e=>{
            e.preventDefault();

            let return_message  = $("#contact_return_message_js");
            let messageR_v      = "Message en cours d'envoi";
            let className_v     = ["my-alert","my-alert-success"];

            let name_v      = $('#name');
            let email_v     = $('#email');
            let subject_v   = $('#subject');
            let message_v   = $('#message');

            if (name_v.val() && email_v.val() && subject_v.val() && message_v.val()) {

                let data_v    = {
                    name:name_v.val(),
                    email:email_v.val(),
                    subject:subject_v.val(),
                    message: message_v.val()
                }
                $.ajax({
                    type: 'POST',
                    url:"/message/contact",
                    data: JSON.stringify(data_v),
                    cache: false,
                    success: function (response) {
                        let return_json = JSON.parse(response);
                            messageR_v  = return_json.message;
                        if (return_json.status == 200) {
                            className_v = ["my-alert","my-alert-success"];
                        }else {
                            className_v = ["my-alert","my-alert-danger"];
                        }
                        return_message.addClass(className_v);
                        return_message.text(messageR_v);
                        cleanContactFormField();

                        setTimeout(e=>{
                            return_message.removeClass(className_v);
                            return_message.text("");
                        },5000);

                    },
                    error: function (error) {
                        console.error(error);
                    },

                });

            }else {
                messageR_v  = "Veuillez remplir tous les champs obligatoires *";
                className_v = ["my-alert","my-alert-danger"];
            }

            return_message.addClass(className_v);
            return_message.text(messageR_v);

            setTimeout(e=>{
                return_message.removeClass(className_v);
                return_message.text("");
            },5000);

        })
    }
}

//TODO - to refact
function cleanContactFormField() {
    let name_v      = $('#name');
    let email_v     = $('#email');
    let subject_v   = $('#subject');
    let message_v   = $('#message');
    name_v.val('');
    email_v.val('');
    subject_v.val('');
    message_v.val('');
}