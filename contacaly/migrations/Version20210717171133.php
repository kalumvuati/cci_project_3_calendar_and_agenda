<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210717171133 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contact_category (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, category VARCHAR(100) NOT NULL, INDEX IDX_9C698556A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contacts (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, user_id INT DEFAULT NULL, names VARCHAR(100) NOT NULL, email VARCHAR(255) DEFAULT NULL, phone VARCHAR(50) NOT NULL, picture VARCHAR(255) DEFAULT NULL, site VARCHAR(100) DEFAULT NULL, address VARCHAR(100) DEFAULT NULL, infos LONGTEXT DEFAULT NULL, jobtitle VARCHAR(50) DEFAULT NULL, created_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_33401573E7927C74 (email), UNIQUE INDEX UNIQ_33401573444F97DD (phone), INDEX IDX_3340157312469DE2 (category_id), INDEX IDX_33401573A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE events (id INT AUTO_INCREMENT NOT NULL, users_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, all_day TINYINT(1) DEFAULT NULL, start_at_date DATETIME NOT NULL, end_at_date DATETIME NOT NULL, description LONGTEXT NOT NULL, picture VARCHAR(255) DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, bgcolor VARCHAR(10) DEFAULT NULL, shared TINYINT(1) NOT NULL, allow_modify TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_5387574A67B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE events_users (events_id INT NOT NULL, users_id INT NOT NULL, INDEX IDX_A43F6DCF9D6A1065 (events_id), INDEX IDX_A43F6DCF67B3B43D (users_id), PRIMARY KEY(events_id, users_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) DEFAULT NULL, name VARCHAR(255) NOT NULL, lastname VARCHAR(100) DEFAULT NULL, last_connection_at DATETIME DEFAULT NULL, subscripted_at DATETIME NOT NULL, phone VARCHAR(50) DEFAULT NULL, picture VARCHAR(255) DEFAULT NULL, infos LONGTEXT DEFAULT NULL, is_verified TINYINT(1) NOT NULL, job_title VARCHAR(50) DEFAULT NULL, updated_at DATETIME DEFAULT NULL, google_sub VARCHAR(255) DEFAULT NULL, facebook_id VARCHAR(255) DEFAULT NULL, github_id VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contact_category ADD CONSTRAINT FK_9C698556A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_3340157312469DE2 FOREIGN KEY (category_id) REFERENCES contact_category (id)');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_33401573A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574A67B3B43D FOREIGN KEY (users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE events_users ADD CONSTRAINT FK_A43F6DCF9D6A1065 FOREIGN KEY (events_id) REFERENCES events (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE events_users ADD CONSTRAINT FK_A43F6DCF67B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE contacts DROP FOREIGN KEY FK_3340157312469DE2');
        $this->addSql('ALTER TABLE events_users DROP FOREIGN KEY FK_A43F6DCF9D6A1065');
        $this->addSql('ALTER TABLE contact_category DROP FOREIGN KEY FK_9C698556A76ED395');
        $this->addSql('ALTER TABLE contacts DROP FOREIGN KEY FK_33401573A76ED395');
        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574A67B3B43D');
        $this->addSql('ALTER TABLE events_users DROP FOREIGN KEY FK_A43F6DCF67B3B43D');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('DROP TABLE contact_category');
        $this->addSql('DROP TABLE contacts');
        $this->addSql('DROP TABLE events');
        $this->addSql('DROP TABLE events_users');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE users');
    }
}
