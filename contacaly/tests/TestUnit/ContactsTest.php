<?php

namespace App\Tests\TestUnit;

use App\Entity\ContactCategory;
use App\Entity\Contacts;
use App\Entity\Users;
use PHPUnit\Framework\TestCase;

class ContactsTest extends TestCase
{
    /**
     * @dataProvider providerIsTrue
     * @param Contacts $Contacts
     */
    public function testIsTrue(Contacts $Contacts): void
    {
        $today = new \DateTime();
        $this->assertEquals("name", $Contacts->getNames());
        $this->assertEquals("email", $Contacts->getEmail());
        $this->assertEquals("phone", $Contacts->getPhone());
        $this->assertEquals("picture", $Contacts->getPicture());
        $this->assertEquals("site", $Contacts->getSite());
        $this->assertEquals("address", $Contacts->getAddress());
        $this->assertEquals("info", $Contacts->getInfos());
        $this->assertEquals("jobtitle", $Contacts->getJobtitle());
        $this->assertInstanceOf(ContactCategory::class, $Contacts->getCategory());
        $this->assertInstanceOf(Users::class, $Contacts->getUser());
        $this->assertEquals($today->format("d"), $Contacts->getCreatedAt()->format("d"));
    }

    /**
     * @dataProvider providerIsTrue
     * @param Contacts $Contacts
     */
    public function testIsFalse(Contacts $Contacts): void
    {
        $today = new \DateTime("+1 day");
        $this->assertNotEquals("false", $Contacts->getNames());
        $this->assertNotEquals("false", $Contacts->getEmail());
        $this->assertNotEquals("false", $Contacts->getPhone());
        $this->assertNotEquals("false", $Contacts->getPicture());
        $this->assertNotEquals("false", $Contacts->getSite());
        $this->assertNotEquals("false", $Contacts->getAddress());
        $this->assertNotEquals("false", $Contacts->getInfos());
        $this->assertNotEquals("false", $Contacts->getJobtitle());
        $this->assertNotInstanceOf(Users::class, $Contacts->getCategory());
        $this->assertNotInstanceOf(ContactCategory::class, $Contacts->getUser());
        $this->assertNotEquals($today->format("d"), $Contacts->getCreatedAt()->format("d"));
    }

    /**
     * @dataProvider providerIsEmpty
     * @param Contacts $Contacts
     */
    public function testIsEmpty(Contacts $Contacts): void
    {
        $this->assertEmpty($Contacts->getNames());
        $this->assertEmpty($Contacts->getEmail());
        $this->assertEmpty($Contacts->getPhone());
        $this->assertEmpty($Contacts->getPicture());
        $this->assertEmpty($Contacts->getSite());
        $this->assertEmpty($Contacts->getAddress());
        $this->assertEmpty($Contacts->getInfos());
        $this->assertEmpty($Contacts->getJobtitle());
        $this->assertEmpty($Contacts->getCategory());
        $this->assertEmpty($Contacts->getUser());
    }

    public function providerIsTrue(): array
    {
        $Contacts = [];
        $today = new \DateTime();
        for ($i = 0; $i < 5; $i++) {
            $contact = new Contacts();
            $contact->setNames("name")
                ->setEmail("email")
                ->setPhone("phone")
                ->setPicture("picture")
                ->setSite("site")
                ->setAddress("address")
                ->setInfos("info")
                ->setJobtitle("jobtitle")
                ->setCategory(new ContactCategory())
                ->setUser(new Users())
                ->setCreatedAt($today);
            $Contacts []= [$contact];
        }
        return $Contacts;
    }

    public function providerIsEmpty(): array
    {
        $Contacts = [];
        for ($i = 0; $i < 5; $i++) {
            $event = new Contacts();
            $Contacts [] = [$event];
        }
        return $Contacts;
    }
}
