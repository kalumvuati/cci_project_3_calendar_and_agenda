<?php

namespace App\Tests\testUnit\Entity;

use App\Entity\Events;
use App\Entity\Users;
use PHPUnit\Framework\TestCase;

class EventsTest extends TestCase
{
    /**
     * @dataProvider providerIsTrue
     * @param Events $Events
     */
    public function testIsTrue(Events $Events): void
    {
        $today = new \DateTime();
        $start = new \DateTime();
        $end = new \DateTime("+1 day");
        $this->assertEquals("title", $Events->getTitle());
        $this->assertEquals(true, $Events->getAllDay());
        $this->assertEquals($start->format("d"), $Events->getStartAtDate()->format("d"));
        $this->assertEquals($end->format("d"), $Events->getEndAtDate()->format("d"));
        $this->assertEquals("description", $Events->getDescription());
        $this->assertEquals("picture", $Events->getPicture());
        $this->assertEquals("location", $Events->getLocation());
        $this->assertInstanceOf(Users::class, $Events->getUsers());
        $this->assertEquals("#fff", $Events->getBgcolor());
        $this->assertEquals(true, $Events->getShared());
        $this->assertEquals(true, $Events->getAllowModify());
        $this->assertEquals($today->format("d"), $Events->getCreatedAt()->format("d"));
        $this->assertInstanceOf(Users::class, $Events->getMembers()->first());
    }

    /**
     * @dataProvider providerIsTrue
     * @param Events $Events
     */
    public function testIsFalse(Events $Events): void
    {
        $today = new \DateTime("+1 day");
        $start = new \DateTime("+2 days");
        $end = new \DateTime("+4 days");
        $this->assertNotEquals("false", $Events->getTitle());
        $this->assertNotEquals(false, $Events->getAllDay());
        $this->assertNotEquals($start->format("d"), $Events->getStartAtDate()->format("d"));
        $this->assertNotEquals($end->format("d"), $Events->getEndAtDate()->format("d"));
        $this->assertNotEquals("false", $Events->getDescription());
        $this->assertNotEquals("false", $Events->getPicture());
        $this->assertNotEquals("false", $Events->getLocation());
        $this->assertNotInstanceOf(Events::class, $Events->getUsers());
        $this->assertNotEquals("#000", $Events->getBgcolor());
        $this->assertNotEquals(false, $Events->getShared());
        $this->assertNotEquals(false, $Events->getAllowModify());
        $this->assertNotEquals($today->format("d"), $Events->getCreatedAt()->format("d"));
        $this->assertNotInstanceOf(Events::class, $Events->getMembers()->first());
    }

    /**
     * @dataProvider providerIsEmpty
     * @param Events $Events
     */
    public function testIsEmpty(Events $Events): void
    {
        $this->assertEmpty($Events->getTitle());
        $this->assertEmpty($Events->getAllDay());
        $this->assertEmpty($Events->getStartAtDate());
        $this->assertEmpty($Events->getEndAtDate());
        $this->assertEmpty($Events->getDescription());
        $this->assertEmpty($Events->getPicture());
        $this->assertEmpty($Events->getLocation());
        $this->assertEmpty($Events->getUsers());
        $this->assertEmpty($Events->getBgcolor());
        $this->assertEmpty($Events->getShared());
        $this->assertEmpty($Events->getAllowModify());
        $this->assertEmpty($Events->getMembers()->first());
    }

    public function providerIsTrue(): array
    {
        $Events = [];
        $today = new \DateTime();
        $start = new \DateTime();
        $end = new \DateTime("+1 day");
        for ($i = 0; $i < 5; $i++) {
            $event = new Events();
            $event->setTitle("title")
                ->setAllDay(true)
                ->setStartAtDate($start)
                ->setEndAtDate($end)
                ->setDescription("description")
                ->setPicture("picture")
                ->setLocation("location")
                ->setUsers(new Users())
                ->setBgcolor("#fff")
                ->setShared(true)
                ->setAllowModify(true)
                ->setCreatedAt($today)
                ->addMember(new Users());
            $Events []= [$event];
        }
        return $Events;
    }

    public function providerIsEmpty(): array
    {
        $Events = [];
        for ($i = 0; $i < 5; $i++) {
            $event = new Events();
            $Events [] = [$event];
        }
        return $Events;
    }
}