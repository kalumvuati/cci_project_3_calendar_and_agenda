<?php

namespace App\Tests\TestUnit;

use App\Entity\ContactCategory;
use App\Entity\Contacts;
use App\Entity\Events;
use App\Entity\Users;
use PHPUnit\Framework\TestCase;

class UsersTest extends TestCase
{
    /**
     * @dataProvider providerIsTrue
     * @param Users $Users
     */
    public function testIsTrue(Users $Users): void
    {
        $update = new \DateTime("+4 days");
        $today = new \DateTime();
        $this->assertEquals("email", $Users->getEmail());
        $this->assertEquals(["ROLE_USER"], $Users->getRoles());
        $this->assertEquals("password", $Users->getPassword());
        $this->assertEquals("plainpassword", $Users->getPlainPasswordConf());
        $this->assertEquals("name", $Users->getName());
        $this->assertEquals("lastname", $Users->getLastname());
        $this->assertEquals("phone", $Users->getPhone());
        $this->assertEquals('picture', $Users->getPicture());
        $this->assertEquals('infos', $Users->getInfos());
        $this->assertEquals(true, $Users->getIsVerified());
        $this->assertEquals("jobtitle", $Users->getJobTitle());
        $this->assertInstanceOf(Events::class, $Users->getMemberOfEvents()->first());
        $this->assertInstanceOf(ContactCategory::class, $Users->getContactCategories()->first());
        $this->assertInstanceOf(Contacts::class, $Users->getContacts()->first());
        $this->assertInstanceOf(Events::class, $Users->getEvents()->first());
        $this->assertEquals($today->format("d"), $Users->getLastConnectionAt()->format("d"));
        $this->assertEquals($today->format("d"), $Users->getSubscriptedAt()->format("d"));
        $this->assertEquals($update->format("d"), $Users->getUpdatedAt()->format("d"));
    }

    /**
     * @dataProvider providerIsTrue
     * @param Users $Users
     */
    public function testIsFalse(Users $Users): void
    {
        $update = new \DateTime("+4 days");
        $today = new \DateTime();
        $this->assertNotEquals("false", $Users->getEmail());
        $this->assertNotEquals(["ROLE_ADMIN"], $Users->getRoles());
        $this->assertNotEquals("false", $Users->getPassword());
        $this->assertNotEquals("false", $Users->getPlainPasswordConf());
        $this->assertNotEquals("false", $Users->getName());
        $this->assertNotEquals("false", $Users->getLastname());
        $this->assertNotEquals("false", $Users->getPhone());
        $this->assertNotEquals('false', $Users->getPicture());
        $this->assertNotInstanceOf(Users::class, $Users->getImageFile());
        $this->assertNotEquals('false', $Users->getInfos());
        $this->assertNotEquals(false, $Users->getIsVerified());
        $this->assertNotEquals("false", $Users->getJobTitle());
        $this->assertNotInstanceOf(Users::class, $Users->getMemberOfEvents()->first());
        $this->assertNotInstanceOf(Users::class, $Users->getContactCategories()->first());
        $this->assertNotInstanceOf(ContactCategory::class, $Users->getContacts()->first());
        $this->assertNotInstanceOf(ContactCategory::class, $Users->getEvents()->first());
        $this->assertNotEquals($update->format("d"), $Users->getLastConnectionAt()->format("d"));
        $this->assertNotEquals($update->format("d"), $Users->getSubscriptedAt()->format("d"));
        $this->assertNotEquals($today->format("d"), $Users->getUpdatedAt()->format("d"));
    }

    /**
     * @dataProvider providerIsEmpty
     * @param Users $Users
     */
    public function testIsEmpty(Users $Users): void
    {
        $this->assertEmpty($Users->getEmail());
        $this->assertEmpty($Users->getPassword());
        $this->assertEmpty($Users->getPlainPasswordConf());
        $this->assertEmpty($Users->getName());
        $this->assertEmpty($Users->getLastname());
        $this->assertEmpty($Users->getPhone());
        $this->assertEmpty($Users->getInfos());
        $this->assertEmpty($Users->getIsVerified());
        $this->assertEmpty($Users->getJobTitle());
        $this->assertEmpty($Users->getMemberOfEvents());
        $this->assertEmpty($Users->getContactCategories());
        $this->assertEmpty($Users->getContacts());
        $this->assertEmpty($Users->getEvents());
        $this->assertEmpty($Users->getLastConnectionAt());
    }

    public function providerIsTrue(): array
    {
        $Users = [];
        $update = new \DateTime("+4 days");
        $today = new \DateTime();
        for ($i = 0; $i < 5; $i++) {
            $user = new Users();
            $user->setEmail("email")
                ->setPassword("password")
                ->setPlainPasswordConf("plainpassword")
                ->setName("name")
                ->setLastname("lastname")
                ->setPhone("phone")
                ->setPicture("picture")
                ->setInfos('infos')
                ->setIsVerified(true)
                ->setJobTitle("jobtitle")
                ->addMemberOfEvent(new Events())
                ->addContactCategory(new ContactCategory())
                ->addContact(new Contacts())
                ->addEvent(new Events())
                ->setLastConnectionAt($today)
                ->setSubscriptedAt($today)
                ->setUpdatedAt($update);
            $Users []= [$user];
        }
        return $Users;
    }

    public function providerIsEmpty(): array
    {
        $Users = [];
        for ($i = 0; $i < 5; $i++) {
            $user = new Users();
            $Users [] = [$user];
        }
        return $Users;
    }
}
