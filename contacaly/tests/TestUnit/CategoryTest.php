<?php

namespace App\Tests\TestUnit;

use App\Entity\ContactCategory;
use App\Entity\Contacts;
use App\Entity\Events;
use App\Entity\Users;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    /**
     * @dataProvider providerIsTrue
     * @param ContactCategory $ContactCategory
     */
    public function testIsTrue(ContactCategory $ContactCategory): void
    {
        $this->assertEquals("category", $ContactCategory->getCategory());
        $this->assertInstanceOf(Contacts::class, $ContactCategory->getContacts()->first());
        $this->assertInstanceOf(Users::class, $ContactCategory->getUser());
    }

    /**
     * @dataProvider providerIsTrue
     * @param ContactCategory $ContactCategory
     */
    public function testIsFalse(ContactCategory $ContactCategory): void
    {
        $this->assertNotEquals("false", $ContactCategory->getCategory());
        $this->assertNotInstanceOf(Users::class, $ContactCategory->getContacts()->first());
        $this->assertNotInstanceOf(Events::class, $ContactCategory->getUser());
    }

    /**
     * @dataProvider providerIsEmpty
     * @param ContactCategory $ContactCategory
     */
    public function testIsEmpty(ContactCategory $ContactCategory): void
    {
        $this->assertEmpty($ContactCategory->getCategory());
        $this->assertEmpty($ContactCategory->getContacts()->first());
        $this->assertEmpty($ContactCategory->getUser());
    }

    public function providerIsTrue(): array
    {
        $ContactCategory = [];
        for ($i = 0; $i < 5; $i++) {
            $contact = new ContactCategory();
            $contact->setCategory("category")
                ->setUser(new Users())
                ->addContact(new Contacts());
            $ContactCategory []= [$contact];
        }
        return $ContactCategory;
    }

    public function providerIsEmpty(): array
    {
        $ContactCategory = [];
        for ($i = 0; $i < 5; $i++) {
            $contact = new ContactCategory();
            $ContactCategory [] = [$contact];
        }
        return $ContactCategory;
    }
}
