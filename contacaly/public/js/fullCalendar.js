var calementEl          = document.querySelector("#calendar");
//Users like members
var members_element     = $(".multiple_select2_js");
//Will stop setting once again members on the list
var member_element_exist= false;
var fullCalendar        = null;

//TODO - to use
const eventForm = {
    id:$('#id'),
    title: $('#title'),
    allday:$('#all_days'),
    start_date:$('#start_at'),
    start_time:$('#start_at_time'),
    end_date:$('#end_at'),
    end_time:$('#end_at_time'),
    description: $('#description'),
    members: $('#members'),
    backgroundColor: $('#backgroundColor'),
    location: $('#location'),
    shared: $('#shared'),
    allow_modify: $('#allow_modify'),
    sabe_btn_element: $('#user_event_add_save_btn_js'),
    save_btn: $('#user_event_add_save_btn_js .fal'),
    delete_btn: $('#user_event_add_delete_btn_js')
}

//Create a fullCalendar
/**
 *
 * @param events : events from database
 * @param solo : bolean for printing calendar without btns on top and less details
 */
function fullCalendarInit(events, solo= false) {

    if (calementEl) {
        //Creating a new calendar instance
        if (solo) {

            fullCalendar = new FullCalendar.Calendar(calementEl,{
            initialView: 'dayGridMonth',
            locale: 'fr',
            timeZone: 'locale',
            allDayText: 'Journée entière',
            nowIndicator: true,
            selectable: true,
            //themeSystem: 'bootstrap', // ça déforme mon CSS
            buttonText : false,
            headerToolbar: false,
            events: events,
        editable: true,
            eventResizableFromStart:true
        });

        }else {
            fullCalendar = new FullCalendar.Calendar(calementEl,{
                initialView: 'timeGridWeek',
                locale: 'fr',
                timeZone: 'locale',
                allDayText: 'Journée entière',
                nowIndicator: true,
                selectable: true,
                //themeSystem: 'bootstrap', // ça déforme mon CSS
                buttonText :{
                    month: "Mois",
                    week: "Semaine",
                    list: "Jour",
                    today: "Aujourd’hui",
                },
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,listDay'
                },
                events: events,
                editable: true,
                eventResizableFromStart:true
            });
        }

        //MEMBER - Ajax request for getting members
        //Users like members
        if (!member_element_exist) {
            multiple_select2_js(members_element);
            member_element_exist = true;
        }

        //EVENT - When the event was changed
        fullCalendar.on("eventChange",event=>{
            //Update user event on database
            updateEvent(event.event);
        });

        //Event on doubleClick on Event to Edit -- ON CHANGE
        fullCalendar.on('eventClick', event => {
            cleanEventFields();
            eventForm.id.val(event.event.id);
            eventForm.title.val(event.event.title);
            if (event.event.extendedProps.shared) {
                eventForm.shared.attr("checked", "checked");
                eventForm.shared.prop("checked",true);
            }
            if (event.event.extendedProps.allow_modify) {
                eventForm.allow_modify.attr("checked", "checked");
                eventForm.allow_modify.prop("checked",true);
            }
            if (event.event.allDay) {
                eventForm.allday.attr("checked", "checked");
                eventForm.allday.prop("checked",true);
                eventForm.end_time.val(getTime(event.event.start));
                eventForm.start_date.val(getDate(event.event.start));

                eventForm.start_time.val(getTime(event.event.start));
                eventForm.end_date.val(getDate(event.event.start));
            }else {
                eventForm.start_date.val(getDate(event.event.start));
                eventForm.start_time.val(getTime(event.event.start));

                eventForm.end_date.val(getDate(event.event.end));
                eventForm.end_time.val(getTime(event.event.end));
            }

            eventForm.description.val(event.event.extendedProps.description);
            eventForm.members.val(event.event.extendedProps.members);
            eventForm.backgroundColor.val(event.event.backgroundColor);
            eventForm.location.val(event.event.extendedProps.location);

            //Remove deelte and edit button
            if (event.event.extendedProps.allow_modify === false) {
                //Remove delete and save button for other users
            } else {
                //SHOW DELETE BTN
                eventForm.delete_btn.css('display','block');
            }
            eventForm.save_btn.removeClass('fa-save');
            eventForm.save_btn.addClass('fa-edit');
            $('.user_event_add').addClass("show");



            //Showing member of this event
            let container_select2_ul = $('#select2-viewer-members_js');

            if (container_select2_ul.length > 0) {
                container_select2_ul.empty();
            }

            for (const member of event.event.extendedProps.members ) {
                let div = document.createElement('div');
                div.classList.add("select2-selection__choice__remove");
                div.innerHTML = `
                        <span for="">x</span>
                        <input disabled="disabled" data-event="${event.event.id}" type="text" name="member_${member.id}" id="${member.id}" value="${member.name}">
                        `;
                container_select2_ul.append(div)
            }

            //Deleting member from an event
            $('.select2-selection__choice__remove').click(e=>{
                //TODO  - to select only cross element
                if ( e.currentTarget.children[1]) {
                    deleteMemberFromEvent(
                        parseInt(e.currentTarget.children[1].dataset.event),
                        parseInt(e.currentTarget.children[1].id),e.currentTarget);
                }
            });
        });

        fullCalendar.on('select',event=>{
            //HIDE DELETE BTN
            $("#user_event_add_delete_btn_js").css('display','none');

            //TODO  - refactoring
            if (!member_element_exist) {
                multiple_select2_js(members_element);
                member_element_exist = true;
            }

            cleanEventFields();

            //DATE AND TIME PICKER
            eventForm.start_date.val(event.startStr.split('T')[0]);
            eventForm.start_time.val(event.startStr.split('T')[1]);
            eventForm.end_date.val(event.endStr.split('T')[0]);
            eventForm.end_time.val(event.endStr.split('T')[1]);

            eventForm.save_btn.removeClass('fa-edit');
            eventForm.save_btn.addClass('fa-save');
            $('.user_event_add').addClass("show");

        });

        fullCalendar.render();

        //EVENT - For create Event Return Button
        $('#user_event_add_return_btn_js').click(e=>{
            //Have to work
            e.preventDefault();
            cleanEventFields();

        });

        //EVENT - For create Event Save Button (AJAX Request)
        $('#user_event_add_save_btn_js').click(e=>{
            e.preventDefault();
            updateEvent();
        });

        //EVENT - For create Event Save Button (AJAX Request)
        $('#user_event_add_delete_btn_js').click(e=>{
            e.preventDefault();
            let event_To_Delete = fullCalendar.getEventById(eventForm.id.val())
            if (event_To_Delete) {
                $.ajax({
                    url: "/users/events/"+eventForm.id.val()+"/delete",
                    type:'POST',
                    date: JSON.stringify({id:eventForm.id.val()}),
                    cache: false,
                    success: function (response) {
                        let returnMassage 	= JSON.parse(response);
                        let class_name 		= "success";
                        if (returnMassage.status === 202) {
                            event_To_Delete.remove();
                            cleanEventFields();
                        }else {
                            class_name = 'warning';
                        }
                        showReturnMessage(returnMassage.message,class_name);
                    },
                    error: function (error) {
                        let error_json = JSON.parse(error.responseText);
                        showReturnMessage(error_json.message,"danger");
                        cleanEventFields();
                    }
                });
            }else{
                cleanEventFields();
            }
        });

        //EVENT - For create event by clicking on nav bar Button
        $('.add_event_js').click(e=>{
            e.preventDefault();
            cleanEventFields();
            //HIDE DELETE BTN
            $("#user_event_add_delete_btn_js").css('display','none');
            $('.user_event_add').addClass("show");
            eventForm.save_btn.removeClass('fa-edit');
            eventForm.save_btn.addClass('fa-save');
        });
    }

}

//TODO - refactoring Ajax functions. - send only data and url
function deleteMemberFromEvent(event_id, member_id, element) {
    $.ajax({
        url: "/events/delete/member",
        type:"POST",
        data: JSON.stringify({event:event_id, member:member_id}),
        cache: false,
        success: function (response) {
            $response_json = response;
            if ($response_json == "success") {
                element.remove();
            }
        },
        error: function (error) {
            console.error(error);
        }
    });
}

//Activate select2 for event's members
$(".multiple_select2_js").select2({
    maximumSelectionLength: 3,
    placeholder: "Ajouter les membres"
});

/**
 * This function will make request to server to get users and adds them in list of members
 * @param members_element : where members will be added as option on select HTML tag
 */
function multiple_select2_js (members_element)
{
    let $users =  null;
    url = "/users/dashboard/calendar/search";
    $.ajax({
        url: url,
        type:"POST",
        data: JSON.stringify({name:"members"}),
        cache: false,
        success: function (response) {
            $users          = JSON.parse(response);
            $users.forEach($user => {
                let option  = document.createElement('option');
                option.classList.add('members-option');
                option.setAttribute('value',$user.id);
                option.innerHTML= `${$user.name} ${$user.lastname}`;
                members_element.append(option);
            });
        },
        error: function (error) {
            console.error(error);
        }
    });
}

/**
 * TODO - to fix when i insert an event without refresh the page and modified it, it will create a new one instead modify
 * Update event when this one was changed by using
 * drag and drop
 * @param fullCalendarEvent
 */
function updateEvent(fullCalendarEvent = null) {

    let mandatoriesFieldsEmpties= false;
    let message     = "";
    let classe_name = "";
    let on_change   = false;
    let ajax_data   = null;
    //For updating an existant event - ONCHANGE
    if (fullCalendarEvent) {
        ajax_data   = fullCalendarEvent.toJSON();
        on_change   = true;
        //This garanties to avoid the duplicate effect on calendar
        //fullCalendarEvent.remove()
    }else {
        //For creating a new event - ONCLICK AND ADD NEW
        //Check de mandatories fields
        if (
            !eventForm.title.val() || !eventForm.start_date.val() ||
            !eventForm.start_time.val() || !eventForm.end_date.val() ||
            !eventForm.end_time.val() || !eventForm.description.val())
        {
            mandatoriesFieldsEmpties = true;
            showError();
        }else {
            //Members id
            let members = [];
            for (let i = 0; i < eventForm.members.val().length; i++) {
                members.push({id: eventForm.members.val()[i]});
            }

            ajax_data = {
                id: eventForm.id.val()?eventForm.id.val():0,
                title: eventForm.title.val(),
                start: `${eventForm.start_date.val()} ${eventForm.start_time.val()}` ,
                end: `${eventForm.end_date.val()} ${eventForm.end_time.val()}`,
                allDay:eventForm.allday.is(":checked")?1:0,
                backgroundColor: eventForm.backgroundColor.val(),
                extendedProps: {
                    description: eventForm.description.val(),
                    members: members,
                    shared: eventForm.shared.is(":checked")?1:0,
                    allow_modify: eventForm.allow_modify.is(":checked")?1:0,
                    location: eventForm.location.val()??'',
                },
            }
        }
    }

    if (ajax_data) {
        $.ajax({
        type: "PUT",
        url: `/users/events/update_ajax/${ajax_data.id}`,
        data: JSON.stringify(ajax_data),
        cache: false,
        success: function (response) {
            let data_converted 	= JSON.parse(response);
            message 			= data_converted.message;
            classe_name 		= "success";
            //TODO - Clean fields
            if (data_converted.status === 200) {
                if (ajax_data.id || on_change) {
                    let current_event = fullCalendar.getEventById(ajax_data.id);
                    current_event.remove();
                }
                ajax_data.id        = data_converted.event.id;
                //Updating event's member
                ajax_data.members   = data_converted.event.members
                fullCalendar.addEvent(ajax_data);
            }else {
                classe_name = "warning";
            }
            showReturnMessage(message,classe_name);
            //Remove the pop up Modal
            cleanEventFields();
        },
        error: function (error) {
            let error_json = JSON.parse(error.responseText);
            cleanEventFields();
            showReturnMessage(error_json.message,"danger");
        }
    });
    }

}

/**
 * Trait date sent by fullCalendar event
 * @param js_date
 * @param separate
 * @returns {*} return the formated date according my will
 */
function getDate(js_date,separate ="-") {
    let day     = js_date.getUTCDate() < 10 ? "0" + js_date.getUTCDate(): js_date.getUTCDate();
    let month   = (js_date.getUTCMonth() + 1) < 10? "0"+(js_date.getUTCMonth() + 1):(js_date.getUTCMonth() + 1);
    let year    = js_date.getUTCFullYear();
    return `${year}-${month}-${day}`;
}

/**
 * Trait time received from fullCalendar event
 *
 * @param js_date
 * @returns {string}
 */
function getTime(js_date) {
    let hour    = js_date.getUTCHours() < 10 ? "0" + js_date.getUTCHours():js_date.getUTCHours();
    let min     = js_date.getUTCMinutes() < 10 ? "0" + js_date.getUTCMinutes():js_date.getUTCMinutes();
    let sec     = js_date.getUTCSeconds() < 10 ? "0"+js_date.getUTCSeconds():js_date.getUTCSeconds();
    return `${hour}:${min}:${sec}`;
}

/**
 * Shows Error for Event form
 */
function showError() {
    let message_element_v = $('#eventFormError_js');
    if (message_element_v) {
        message_element_v.text("Remplissez au moins les champs obligatoires")
        message_element_v.addClass('show')
        setTimeout(e=>{
            message_element_v.text("")
            message_element_v.removeClass('show')
        },5000)
    }
}

/**
 * Remove Error message set before for Event form
 */
function removeError() {
    let message_element_v = $('#eventFormError_js');
    if (message_element_v) {
        message_element_v.text('');
        message_element_v.removeClass('show');
    }
}

/**
 * cleans all fields form of Event Form
 */
function cleanEventFields() {
    $('.user_event_add').removeClass("show");
    eventForm.id.val('');
    eventForm.title.val('');

    //Best way to remove checked attribute
    eventForm.allday.prop("checked",false);
    eventForm.shared.prop("checked",false);
    eventForm.allow_modify.prop("checked",false);

    eventForm.start_date.val('');
    eventForm.start_time.val('');
    eventForm.end_date.val('');
    eventForm.end_time.val('');
    eventForm.description.val('');
    eventForm.members.val('');
    eventForm.location.val('');
    //Delete selected members
    $(".select2-selection__rendered").html("");
    //Delete old selected members
    $("#select2-viewer-members_js").html("");
    $('#user_event_add_save_btn_js').removeClass('fa-save');
    $('#user_event_add_save_btn_js').removeClass('fa-edit');
}

/**
 * Show return message when event was modified/created by user
 * @param message_p
 * @param class_name_p
 */
function showReturnMessage(message_p,class_name_p) {
    let div_toast = $("#eventReturnMessage");
    div_toast.html(`<div class="my-alert my-alert-${class_name_p}-outline">${message_p}</div>`);
    div_toast.addClass("show");
    window.setTimeout(e=>{
        div_toast.removeClass("show");
    },5000);
}

