<?php

namespace App\Repository;

use App\Entity\Events;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Events|null find($id, $lockMode = null, $lockVersion = null)
 * @method Events|null findOneBy(array $criteria, array $orderBy = null)
 * @method Events[]    findAll()
 * @method Events[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Events::class);
    }

    /**
     * find user's events
     * @param $users_id
     * @return int|mixed|string
     */
    public function findByUsers($users_id)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.users = :users')
            ->setParameter('users', $users_id)
            ->orderBy('e.start_at_date','ASC')
            ->getQuery()->getResult();
    }

    /**
     * find current user's events and incoming events
     * @param $users_id
     * @return int|mixed|string
     */
    public function findByUsersCurrent($users_id)
    {
//        $from   = new DateTime("now",new DateTimezone("Europe/Paris"));
        $to     = new DateTime("2 day",new DateTimezone("Europe/Paris"));

        return $this->createQueryBuilder('e')
            ->where('e.users = :users')
            ->setParameter('users', $users_id)
            ->andWhere('e.start_at_date >= CURRENT_DATE()')
            ->andWhere('e.start_at_date < :to')
            ->setParameter('to', $to->format('Y-m-d'))
            ->orderBy('e.start_at_date','ASC')
            ->getQuery()->getResult();
    }

}
