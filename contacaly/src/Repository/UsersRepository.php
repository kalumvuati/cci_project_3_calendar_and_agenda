<?php

namespace App\Repository;

use App\Entity\Users;
use App\Helper\EmailSender;
use App\Security\EmailVerifier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Client\Provider\GithubResourceOwner;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use Omines\OAuth2\Client\Provider\GitlabResourceOwner;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Message;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Users|null find($id, $lockMode = null, $lockVersion = null)
 * @method Users|null findOneBy(array $criteria, array $orderBy = null)
 * @method Users[]    findAll()
 * @method Users[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    private ParameterBagInterface $bag;
    private EmailVerifier $emailVerifier;

    public function __construct(ManagerRegistry $registry, ParameterBagInterface $bag, EmailVerifier $emailVerifier)
    {
        parent::__construct($registry, Users::class);
        $this->bag = $bag;
        $this->emailVerifier = $emailVerifier;
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     * @param UserInterface $user
     * @param string $newEncodedPassword
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof Users) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findOrCreateFromOauth(ResourceOwnerInterface $owner)
    {
        /** @var Users $user */
        $user = $this->createQueryBuilder('u')
            ->where('u.googleSub = :googleSub')
            ->setParameter('googleSub', $owner->getId())
            ->getQuery()->getOneOrNullResult();

        if ($user) {
            $user->setLastConnectionAt(new \DateTime());
            $this->_em->flush();
            return $user;
        }
        $user_info = $owner->toArray();
        $user = new Users();
        $user->setGoogleSub($owner->getId())
            ->setEmail($user_info['email'])
            ->setName($user_info['family_name'])
            ->setLastname($user_info['given_name'])
            ->isVerified($user_info['email_verified']);
        $this->_em->persist($user);
        $this->_em->flush();

        //Send Email to this new user subscribed by google account
        if (!$user_info['email_verified']) {
            $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                (new TemplatedEmail())
                    ->from(new Address($this->bag->get('email.messager'), 'Tutofree - support service'))
                    ->to($user->getEmail())
                    ->subject('Veuillez confirmer votre adresse mail')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );
        }

        return $user;
    }
}
