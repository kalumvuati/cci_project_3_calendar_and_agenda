<?php

namespace App\Repository;

use App\Entity\Contacts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Contacts|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contacts|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contacts[]    findAll()
 * @method Contacts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contacts::class);
    }

    /**
     * Does searches on database according options given
     * @param array $options
     * @return array
     */
    public function findByOptions(Array $options): array
    {
        $query          = $this->createQueryBuilder('c');

        //By Category
        if (isset($options["category"])) {
            $query->where('c.category  = :category_id')
                ->setParameter('category_id',$options["category"]);
        }
        //by  Member's names
        if (isset($options['names'])) {
            $param = strtolower($options["names"]);
            $query->orWhere('lower(c.names) LIKE :names')
                ->setParameter('names',"%$param%");
        }

        //By phone number
        if (isset($options['phone'])) {
            $param = strtolower($options["phone"]);
            $query->orWhere('lower(c.phone) LIKE :phone')
                ->setParameter('phone',"%$param%");
        }

        $query->addOrderBy('c.names', 'ASC');
        return $query->getQuery()->getResult();
    }

    public function findAddedToday(int $limit = 5)
    {

        return $this->createQueryBuilder('c')
            ->andWhere('c.created_at >= CURRENT_DATE()')
            ->orderBy('c.id', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

}
