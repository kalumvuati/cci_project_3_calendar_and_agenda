<?php

namespace App\Form;

use App\Entity\ContactCategory;
use App\Entity\Contacts;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('names', TextType::class, [
                'label' => 'Nom complets *'
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => false
            ])
            ->add('phone', TelType::class, [
                'label' => 'Téléphone *'
            ])
            ->add('site', TextType::class, [
                'label' => 'Adresse Url du site web',
                'required' => false
            ])
            ->add('address', TextType::class, [
                'label' => 'Adresse',
                'required' => false
            ])
            ->add('infos', TextareaType::class, [
                'label' => "Description de l'entreprise",
                'required' => false
            ])
            ->add('jobtitle', TextType::class, [
                'label' => 'Poste',
                'required' => false
            ])
            ->add('category', EntityType::class,
                [
                    'class'=> ContactCategory::class,
                    'choice_label'=>"category"
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contacts::class,
        ]);
    }
}
