<?php

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',EmailType::class,
                [
                    'label' => 'Email'
                ],
            )
            ->add('name',TextType::class,
                [
                    'label' => 'Nom'
                ],
            )
            ->add('lastname',TextType::class,
                [
                    'label' => 'Prénom'
                ],
            )->add('imageFile',FileType::class,
                [
                    'label' => 'Photo de profil',
                    'required'=>false
                ],
            )
            ->add('phone',TelType::class,
                [
                    'label' => 'Téléphone',
                    'required'=>false
                ],
            )
            ->add('jobtitle',TextType::class,
                [
                    'label' => 'Métier',
                    'required'=>false
                ],
            )
            ->add('infos',TextareaType::class,
                [
                    'label' => 'Description',
                    'required'=>false
                ],
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}
