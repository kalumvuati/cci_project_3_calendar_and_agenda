<?php

namespace App\Form;

use App\Entity\Events;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Time;

class EventsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',TextType::class, ['label' => 'Titre*'])
            ->add('all_day', CheckboxType::class,
                [
                    'label' => 'Toute la journée*',
                    'required' => false
                ])
            ->add('shared', CheckboxType::class,
                [
                    'label' => "Partagez" ,
                    'required' => false
                ])
            ->add('start_at_date', DateTimeType::class,[
                'label' => false,
                'date_widget'=>'single_text'
            ])
            ->add('end_at_date',DateTimeType::class,[
                'label' => false,
                'date_widget'=>'single_text'
            ])
            ->add('description',TextareaType::class,['label' => "Description*"])
            ->add('picture',TextType::class,
                [
                    'label' => 'Ajouter une photo',
                    'required' => false
                ])
            ->add('location',TextType::class,
                [
                    'label' => "Ajouter le Lieu",
                    'required' => false

                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Events::class,
        ]);
    }
}
