<?php

namespace App\DataFixtures;

use App\Entity\ContactCategory;
use App\Entity\Contacts;
use App\Entity\Users;
use App\Repository\ContactCategoryRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ContactsFixtures extends Fixture
{
    /**
     * @var ContactCategoryRepository
     */
    private ContactCategoryRepository $contactCategory;

    public function __construct(ContactCategoryRepository $categoryRepository)
    {
        $this->contactCategory = $categoryRepository;
    }

    public function load(ObjectManager $manager)
    {
        //Faker init
        $faker      = Factory::create('fr_FR');
        $categories = ['Client','Collaborateur',"Fournisseur","Livreur"];

        //FAker Categories
        $contacCategories_size = count($categories);
        for ($i = 0 ; $i < $contacCategories_size; $i++) {
            $category = new ContactCategory();
            $category->setCategory($categories[$i]);
            $manager->persist($category);
            $nb_contact = $faker->numberBetween(10,20);
            //Faker contacts
            for ($j = 0; $j < $nb_contact; $j++) {
                $contact = new Contacts();
                $contact->setNames($faker->name())
                    ->setEmail($faker->companyEmail())
                    ->setPhone(substr($faker->phoneNumber(),0,19))
                    ->setSite($faker->freeEmailDomain())
                    ->setAddress($faker->streetAddress())
                    ->setInfos($faker->realText(300))
                    ->setJobtitle($faker->jobTitle())
                    ->setCategory($category);

                $manager->persist($contact);
            }
        }
        $manager->flush();

    }

}
