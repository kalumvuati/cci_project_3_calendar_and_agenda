<?php

namespace App\DataFixtures;

use App\Entity\Events;
use App\Entity\Users;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsersFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->passwordEncoder = $userPasswordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $nb_contact = $faker->numberBetween(15,20);
        //Faker init
        $user_admin = new Users();
        $user_admin->setName("KALUMVUATI")
            ->setLastname("Duramana")
            ->setRoles(['ROLE_USER','ROLE_ADMIN'])
            ->setPassword($this->passwordEncoder->encodePassword($user_admin,'admin'))
            ->setEmail("admin@tft.fr");

        $manager->persist($user_admin);

        for ($i = 0 ; $i <  $nb_contact ; $i++) {
            $user = new Users();
            $user->setName($faker->firstName())
                ->setLastname($faker->lastName())
                ->setRoles(($i % 2 == 0) ? ['ROLE_USER'] : ['ROLE_USER', 'ROLE_ADMIN'])
                ->setPassword($this->passwordEncoder->encodePassword($user, "admin"))
                ->setEmail($faker->freeEmail())
                ->setInfos($faker->realText(200))
                ->setJobTitle($faker->jobTitle())
                ->setPhone(substr($faker->phoneNumber(),0,30));

            $manager->persist($user);
            $nb_contact = $faker->numberBetween(5, 10);

            for ($j = 0; $j < $nb_contact; $j++) {
                $event  = new Events();
                $h = $faker->numberBetween(0,23);
                $m =$faker->numberBetween(0,59) ;
                $start  = new DateTime("now",new DateTimezone("Europe/Paris"));
                $end    = new DateTime("now",new DateTimezone("Europe/Paris"));
                //TODO - manager time and date.
                $start->setTime($h,$m,0);
                $end->setTime($h,$m+$faker->numberBetween(0,60),0);

                $event->setTitle($faker->realText(30))
                    ->setAllDay($faker->boolean())
                    ->setDescription($faker->realText(20))
                    ->setShared($faker->boolean())
                    ->setAllowModify($faker->boolean())
                    ->setStartAtDate($start)
                    ->setEndAtDate($end)
                    ->setUsers($user);

                $manager->persist($event);
            }
            $manager->flush();
        }
    }

}
