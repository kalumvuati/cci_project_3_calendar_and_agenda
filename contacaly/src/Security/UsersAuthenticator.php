<?php

namespace App\Security;

use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface as Em;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface as Url;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface as Encoder;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface as Csrf;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class UsersAuthenticator extends AbstractFormLoginAuthenticator implements PasswordAuthenticatedInterface
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private Em $entityManager;
    private Url $urlGenerator;
    private Csrf $csrfTokenManager;
    private Encoder $passwordEncoder;

    public function __construct(Em $entity, Url $url, Csrf $csrf, Encoder $encoder)
    {
        $this->entityManager = $entity;
        $this->urlGenerator = $url;
        $this->csrfTokenManager = $csrf;
        $this->passwordEncoder = $encoder;
    }

    public function supports(Request $request)
    {
        return self::LOGIN_ROUTE === $request->attributes->get('_route') && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $csrf = $request->request->get('_csrf_token');
        $password = $request->request->get('password');
        $credentials = ['email' => $request->request->get('email'), 'password' => $password, 'csrf_token' => $csrf];
        $request->getSession()->set(Security::LAST_USERNAME, $credentials['email']);

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        try {
            $email = ['email' => $credentials['email']];
            $user = $this->entityManager->getRepository(Users::class)->findOneBy($email);
        } catch (\Throwable $th) {
            $message = "Votre serveur n'est pas disponible pour le moment. Veuillez tenter plus tard. Raison : ";
            $message .= $th->getMessage();
            throw new NotFoundHttpException($message);
        }

        if (!$user) {
            // fail authentication with a custom error
            throw new CustomUserMessageAuthenticationException('Impossible de trouver votre email.');
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string
    {
        return $credentials['password'];
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        $userReposiroty = $this->entityManager->getRepository(Users::class);
        $email = $request->get("email");
        /** @var Users $user */
        $user = $userReposiroty->findOneBy(['email'=>$email]);
        if ($user) {
            //TODO - it is updated when this function is called and user exists
            $user->setLastConnectionAt(new \DateTime());
            $this->entityManager->flush();
        }
        return new RedirectResponse($this->urlGenerator->generate('users-dashboard'));
    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}
