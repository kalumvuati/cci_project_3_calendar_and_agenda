<?php


namespace App\Security;


use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class GoogleAuthenticator extends SocialAuthenticator
{

    private ClientRegistry $clientRegistry;
    private EntityManagerInterface $manager;
    private RouterInterface $router;
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(UrlGeneratorInterface$urlGenerator, ClientRegistry $clientRegistry, EntityManagerInterface $manager, RouterInterface $router)
    {
        $this->clientRegistry = $clientRegistry;
        $this->manager = $manager;
        $this->router = $router;
        $this->urlGenerator = $urlGenerator;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse($this->router->generate('app_login'));
    }

    public function supports(Request $request)
    {
        return 'login_google' === $request->attributes->get('_route');
    }

    public function getCredentials(Request $request)
    {
        return $this->fetchAccessToken($this->getClient('google_main'));
    }

    /**
     * @param AccessToken $credentials
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $googleClient = $this->getClient('google_main')->fetchUserFromToken($credentials);
        return $this->manager->getRepository(Users::class)->findOrCreateFromOauth($googleClient);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new RedirectResponse($this->urlGenerator->generate('app_login'));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        return new RedirectResponse($this->urlGenerator->generate('users-dashboard'));
    }

    private function getClient(string $client)
    {
        return $this->clientRegistry->getClient($client);
    }
}