<?php


namespace App\Helper;


use App\Entity\Events;
use App\Entity\Users;
use DateTime;
use Mailgun\Mailgun;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class EmailSender extends AbstractController
{
    const DAYS      = ["Mon"=>"Lundi", "Tue"=>"Mardi","Wed" =>"Mercredi","Thu"=> "Jeudi","Fri"=>"Vendredi","Sat"=>"Samedi","Sun"=>"Dimanche"];
    const MONTHS    = [
        "Jan"=>"Jeanvier",
        "Feb"=>"Fevrier",
        "Mar" =>"Mars",
        "Apr"=> "Avril",
        "May"=>"Mai",
        "Jun"=>"juin",
        "Jul"=>"Juillet",
        "Aug"=>"Août",
        "Sep"=>"Septembre",
        "Oct"=>"Octobre",
        "Nov"=>"Novembre",
        "Dec"=>"Decembre",
    ];
    /**
     * @param MailerInterface $mailer
     * @param Users $from
     * @param Users $to
     * @param string $subject
     * @param Events|null $event
     * @param string $template
     * @throws TransportExceptionInterface
     */
    public static function sendMail(
        MailerInterface $mailer,
        Users $from,
        Users $to,
        string $subject,
        Events $event = null,
        string $template
    ): void
    {

        $email_member = new TemplatedEmail();
        $email_member->from(new Address($from->getEmail(), $from->getName()))
            ->to(new Address($to->getEmail(), $to->getName()))
            ->subject($subject)
            ->htmlTemplate("email/$template.html.twig")
            ->context([
                'names'=>$to->getName()." ".$to->getLastname(),
                "event"=>$event,
            ]);

        $mailer->send($email_member);
    }

    /**
     * @param MailerInterface $mailer
     * @param Users $from
     * @param Users $to
     * @param string $subject
     * @param string $message
     * @throws TransportExceptionInterface
     */
    public static function sendMailContact(
        MailerInterface $mailer,
        Users $from,
        Users $to,
        string $subject,
        string  $message
    ): void
    {

        $email_member = new TemplatedEmail();
        $email_member->from(new Address($from->getEmail(), $from->getName()))
            ->to(new Address($to->getEmail(), $to->getName()))
            ->subject($subject)
            ->htmlTemplate("email/contact.html.twig")
            ->context([
                'names'=>$to->getName(),
                "message"=>$message,
                "from"=>$from->getEmail(),
                "to"=>$to->getEmail(),
                "subject"=>$subject
            ]);

        $mailer->send($email_member);
    }

    /**
     * Sending email for all other reason
     * @param MailerInterface $mailer
     * @param Users $from
     * @param Users $to
     * @param string $subject
     * @param string $message
     * @param string $template
     * @throws \Exception
     */
    public static function sendMailGeneric(
        MailerInterface $mailer,
        Users $from,
        Users $to,
        string $subject,
        string  $message,
        string $template
    ): void
    {
        $email = new TemplatedEmail();
        $email->from(new Address($from->getEmail(), 'Contacaly.') )
            ->to(new Address($to->getEmail(),$to->getName()))
            ->subject($subject)
            ->htmlTemplate("email/$template.html.twig")
            ->context([
                'from'=>$from,
                'to'=>$to,
                'subject'=>$subject,
                'dateSent'=>new \DateTime('now', new \DateTimeZone('europe/paris')),
                'message'=>$message
            ]);
    }

    /**
     * Sending mail by mailgun
     * @param string $mailgun_api_key
     * @param string $mailgun_domain
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $message
     */
    public static function  sendMailGunSimple(
        string $mailgun_api_key,
        string $mailgun_domain,
        string $from,
        string $to,
        string $subject,
        string  $message
    )
    {
        // First, instantiate the SDK with your API credentials
        $mg     = Mailgun::create($mailgun_api_key, 'https://api.eu.mailgun.net'); // For EU servers
        $params = [
            'from'    => $from,
            'to'      => $to,
            'subject' => $subject,
            'text'    => $message
        ];
        $mg->messages()->send($mailgun_domain, $params);
    }
}