<?php

namespace App\Controller\Admin;

use App\Controller\Users\UsersController;
use App\Entity\Events;
use App\Entity\Users;
use App\Form\UsersType;
use App\Repository\EventsRepository;
use App\Repository\UsersRepository;
use App\Security\EmailVerifier;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class AdminUsersController extends UsersController
{
    public function __construct(
        EventsRepository $eventsRepository,
        EntityManagerInterface $entityManager,
        Security $security,
        EmailVerifier $emailVerifier,
        UsersRepository $usersRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        TokenStorageInterface $tokenStorage,
        SessionStorageInterface $sessionStorage
    )
    {
        parent::__construct(
            $eventsRepository,
            $entityManager,
            $security,
            $emailVerifier,
            $usersRepository,
            $passwordEncoder,
            $tokenStorage,
            $sessionStorage
        );
    }

    /**
     * @Route("/admin/users", name="admin-users-index")
     * @param UsersRepository $usersRepository
     * @return Response
     */
    public function index(UsersRepository $usersRepository): Response
    {
        return $this->render("admin/users/index.html.twig",
            [
                'users'=>$usersRepository->findAll(),
                'users_active' => 'active'
            ]
        );
    }

    /**
     * @Route("/admin/users/edit/{id}", name="admin-users-edit", methods={"GET","POST"})
     * @Route("/admin/users/add", name="admin-users-add", methods={"GET","POST"})
     * @param int|null $id
     * @param Request $request
     * @return Response
     */
    public function edit(?int $id, Request $request): Response
    {
        $message        = "Création du nouvel utilisateur  avec succès!";
        $button_message = "save";
        if (isset($id)) {
            $user = $this->usersRepository->find($id);
            $message        = "Modification d'utilisateur avec succès!";
            $button_message = "edit";
        }else {
            $user = new Users();
            $user->setPassword($this->passwordEncoder->encodePassword($user,"@"));
        }

        $form_b = $this->createForm(UsersType::class ,$user);
        $form_b->handleRequest($request);
        if ($form_b->isSubmitted() && $form_b->isValid()) {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->addFlash('success', $message);

            //Sending email to user
            if ($button_message === "save") {
                // generate a signed url and email it to the user
                //TODO - have to verify is this message was sent
                $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                    (new TemplatedEmail())
                        ->from(new Address('filotyty@gmail.com', 'Tutofree - support service'))
                        ->to($user->getEmail())
                        ->subject('Veuillez confirmer votre adresse email, et Réinitialiser votre mot de passe')
                        ->htmlTemplate('registration/confirmation_email.html.twig')
                );
            }
            return $this->redirectToRoute('admin-users-index');
        }
        $form = $form_b->createView();
        $url = $this->generateUrl("admin-users-index");

        return $this->render("users/users/edit.html.twig",
        compact('user','form','button_message','url'));
    }

}
