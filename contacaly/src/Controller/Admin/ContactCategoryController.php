<?php

namespace App\Controller\Admin;

use App\Form\ContactCategoryType;
use App\Entity\ContactCategory;
use App\Repository\ContactCategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactCategoryController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var ContactCategoryRepository
     */
    private ContactCategoryRepository $contact_category_repository;

    public function __construct(
        EntityManagerInterface $entityManager,
        ContactCategoryRepository $repository)
    {
        $this->entityManager = $entityManager;
        $this->contact_category_repository = $repository;
    }

    /**
     * @Route("/admin/categories", name="admin-category-index")
     */
    public function index(): Response
    {
        $categories = $this->contact_category_repository->findAll();
        return $this->render('admin/contact_category/index.html.twig', [
            'categories' => $categories,
            "category_active"=>'active'
        ]);
    }

    /**
     * @Route("/admin/categories/new", name="admin-category-new", methods={"POST", "GET"})
     * @Route("/admin/categories/edit/{id}", name="admin-category-edit", methods={"POST", "GET"})
     */
    public function new(?int $id, Request $request): Response
    {
        $button_message = "save";
        $message        = "Création de nouvelle catégorie avec succès";
        if (isset($id)) {
            $category       = $this->contact_category_repository->find($id);
            $button_message = "edit";
            $message        = "Modification de la catégorie avec succès";
        }else {
            $category = new ContactCategory();
        }

        $form = $this->createForm(ContactCategoryType::class,$category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($category);
            $this->entityManager->flush();
            $this->addFlash('success',$message);

            return $this->redirectToRoute('admin-category-index');
        }

        $categories = $this->contact_category_repository->findAll();
        return $this->render('admin/contact_category/edit.html.twig', [
            'form' => $form->createView(),
            'button_message' => $button_message,
        ]);
    }

    /**
     * @Route("/admin/categories/delete/{id}", name="admin-category-delete", methods={"POST", "DELETE"})
     */
    public function delete(int $id, Request $request): Response
    {
        $category = $this->contact_category_repository->find($id);
        if ($category && $this->isCsrfTokenValid("delete" . $category->getId(), $request->get('_token'))) {

            $this->entityManager->remove($category);
            $this->entityManager->flush();
            $this->addFlash("success", "Suppression avec succès");
        }
        return $this->redirectToRoute("admin-category-index");
    }

}
