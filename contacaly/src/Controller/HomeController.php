<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/ugc", name="ugc", methods="GET")
     * @return mixed
     */
    public function ugc()
    {
        return $this->render("home/ugc.html.twig");
    }

    /**
     * @Route("/mentions", name="mentions", methods="GET")
     * @return mixed
     */
    public function mentions()
    {
        return $this->render("home/mentions.html.twig");
    }

    /**
     * @Route("/tutoriel/{slug}", name="tutoriel", methods="GET")
     * @return mixed
     */
    public function tutoriel(string $slug, Request $request)
    {
        $videos_name = ['event'=>"event",'profile'=>"profile",'contact'=>"contact"];
        $title = "";
        $url = "";
        if ($slug) {
            $title = $videos_name[$slug] ??'';
            $url = 'movies/tutoriel/' . $videos_name[$slug] . '.webm'?? '';
        }

        if (empty($title)) {
            return $this->redirectToRoute('home');
        }
        return $this->render("home/tutoriel.html.twig",
            [
                'title'=>$title,
                'url'=>$url
            ]
        );
    }


}
