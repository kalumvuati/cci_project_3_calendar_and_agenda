<?php

namespace App\Controller\Users;

use App\Entity\Contacts;
use App\Entity\Events;
use App\Entity\Users;
use App\Form\EventsType;
use App\Helper\EmailSender;
use App\Repository\ContactsRepository;
use App\Repository\EventsRepository;
use App\Repository\UsersRepository;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Mailgun\Mailgun;
use Swift_Message;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

//API - CLASS
class EventsController extends AbstractController
{

     /**
     * @var EventsRepository
     */
    private EventsRepository $eventsRepository;

    /**
     * @var UsersRepository
     */
    private UsersRepository $usersRepository;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var Security
     */
    private Security $security;
    private ValidatorInterface $validator;

    public function __construct(
        EventsRepository $repository,
        UsersRepository $usersRepository,
        EntityManagerInterface $entityManager,
        Security $security,
        ValidatorInterface $validator
    )
    {
        $this->eventsRepository = $repository;
        $this->usersRepository  = $usersRepository;
        $this->entityManager    = $entityManager;
        $this->security         = $security;
        $this->validator = $validator;
    }


    /**
     * @Route("/users/calendar/{?id}", name="users-calendar", methods={"GET"})
     * @param Users|null $user
     * @param ContactsRepository $contactsRepository
     * @return Response
     * @throws Exception
     */
    public function calendar(
        ?Users $user = null,
        ContactsRepository $contactsRepository
    ): Response
    {
        if (!$user) {
            /** @var Users $user */
            $user = $this->getUser();
        }

        /**
         * Search for the current User's events by id
         * @var Events [] $events
         */
        $events         = $this->eventsRepository->findByUsers($user->getId());
        $events_json    = [];
        $today          = new DateTime("now",new DateTimezone("Europe/Paris"));
        $nearest_events = $this->eventsRepository->findByUsersCurrent($user);

        /**
         * Contacts added today
         * @var Contacts [] $contacts
         */
        $contacts       = $contactsRepository->findAddedToday(20);

        /** @var Events $event */
        foreach ($events as $event)
        {
            //Check who can see the event : owner, member, shared event
            if (
                $user->getId() === $event->getUsers()->getId() ||
                $event->getShared() ||
                $event->getMembers()->contains($this->getUser())
            )
            {
                //Convert Event PHP Object to Json
                $events_json [] = EventsController::eventToJson($event);
            }
        }

        //Change event color according being Event's owner or Event's member
        foreach ($user->getMemberOfEvents() as $likeMember) {
            $likeMember->setBgcolor("#909399");
            $events_json [] = EventsController::eventToJson($likeMember);
        }

        return $this->render("users/calendar.html.twig",
            [
                'nearest_events'=>$nearest_events,
                'events' =>json_encode($events_json),
                'today' =>[
                    "day"=>$today->format('d'),
                    "days"=>EmailSender::DAYS[$today->format('D')],
                    "month"=>EmailSender::DAYS[$today->format('M')]??$today->format('M'),
                    "year"=>$today->format('Y'),
                ],
                'calendar_active' => 'active',
                "contacts"=>$contacts
            ]
        );
    }

    /**
     * Only owner have granted to delete the event and
     * member but just if the allow modify is activated
     * @Route("/users/events/{id}/delete", name="users-events-delete", methods={"POST|DELETE"})
     * @param int $id
     * @return Response
     */
    public function delete(int $id): Response
    {
        //TODO - Have to check if this user is the event owner
        $event      = $this->eventsRepository->find($id);
        $message    = [
            'message'=>"Vous n'avez pas le droit de supprimer cet événement",
            'status'=>Response::HTTP_FORBIDDEN,
        ];

        if (
            $event &&
            (
                $event->getUsers()->getId() === $this->getUser()->getId() ||
                ($event->getMembers()->contains($this->security->getUser()) && $event->getAllowModify())
            )
        )
        {

            $this->entityManager->remove($event);
            $this->entityManager->flush();

            $message    = [
                'message'=>"Suppression de l'événement avec succès",
                'status'=>Response::HTTP_ACCEPTED,
            ];
        }

        return new Response(json_encode($message),$message['status']);
    }

    /**
     * Any user can make crud.
     * Member can modify event marked if the allow modify is activated
     * @Route("/users/events/update_ajax/{id}", name="users-events-update_ajax", methods="PUT")
     * @param int $id
     * @param Request $request
     * @param UsersRepository $usersRepository
     * @param MailerInterface $mailer
     * @return Response
     * @throws Exception|TransportExceptionInterface
     */
    public function update_ajax( int $id, Request $request, UsersRepository $usersRepository, MailerInterface $mailer)
    {
        $event_data = json_decode($request->getContent());
        $event      = $this->eventsRepository->find($id);
        $message    = [
            'message'=>"Vous n'avez pas le droit de modifier cet événement",
            'status'=>Response::HTTP_METHOD_NOT_ALLOWED,
        ];
        $isToUpdate = true;
        if (!$event) {
            $event = new Events();
            $event->setUsers($this->getUser());
            $isToUpdate = false;
            $message    = [
                'message'=>"Création de événement dans votre 'calendrier' avec succès",
                'status'=> Response::HTTP_OK
            ];
        }

        //wheck if this user is the event's owner or has right to edit it
        if ($event->getUsers()->getId() === $this->getUser()->getId() || $event->getAllowModify())
        {
            if ($event_data && $event && !empty($event_data->title) &&
                !empty($event_data->extendedProps->description) && !empty($event_data->start))
            {

                //Hydrating data
                $event->setTitle($event_data->title)
                    ->setDescription($event_data->extendedProps->description)
                    ->setStartAtDate(new DateTime($event_data->start))
                    ->setShared($event_data->extendedProps->shared)
                    ->setAllowModify($event_data->extendedProps->allow_modify)
                    ->setLocation($event_data->extendedProps->location);
                /**
                 * This condition will avoid saving the background color given to distinct that if event belongs to
                 * the current user or in which current user is member,
                 */
                if ($event_data->backgroundColor != "#909399") {
                    $event->setBgcolor($event_data->backgroundColor);
                }

                if (isset($event_data->allDay)) {
                    $event->setAllDay($event_data->allDay);
                    //the same day at mid-night
                    $event->setEndAtDate(new DateTime($event_data->end));
                }else {
                    $event->setEndAtDate(new DateTime($event_data->end));
                }

                $violations = $this->validator->validate($event);
                if (count($violations)) {
                    $message['status'] = Response::HTTP_UNAUTHORIZED;
                    $message['message'] = '';
                    /** @var ConstraintViolation $violation */
                    foreach ($violations as $violation) {
                        $message['message'] .= $violation->getMessage();
                    }
                } else {

                    //checking event's members
                    foreach ($event_data->extendedProps->members as $member_id)
                    {
                        //Stop the current or creator of event to be a member of his/her own event
                        if ($member_id->id != $this->getUser()->getId()) {
                            $member = $usersRepository->find($member_id->id);
                            if ($member) {
                                if ($event->setMember($member)) {

                                    $message_m  = "Vous avez été invité à un événement tant que membre. 
                                    Ce même événement aura lieu du ". $event->getStartAtDate()->format('d-m-Y à H:i')
                                        . " au ". $event->getEndAtDate()->format('d-m-Y à H:i') .
                                        ($event->getLocation()? " à " .$event->getLocation().'.':".");

                                    EmailSender::sendMail($mailer,$event->getUsers(), $member,
                                        'Invitation du membre', $event,'member');

                                }
                            }
                        }
                    }

                if (!$isToUpdate) {
                    $this->entityManager->persist($event);
                } else {
                    $message        = [
                        'message'=>"Modification de l'événement avec succès",
                        'status'=>Response::HTTP_OK
                    ];
                }
                $this->entityManager->flush();
                $message['event']   = self::eventToJson($event);

            }else {
                $message['message'] = "Pas assez de données";
                $message['status']  =  Response::HTTP_PARTIAL_CONTENT;
            }
        }

        return new Response(json_encode($message), $message['status']);
    }

    /**
     * @param Events $event
     * @return array
     */
    public static function eventToJson(Events $event): array
    {
        $events_json = [];
        if ($event) {
            $members = [];
            /** @var Users $member */
            foreach ($event->getMembers() as $member) {
                $members [] = [
                    'id'=>$member->getId(),
                    'name'=>$member->getName() . " " . $member->getLastname()
                ];
            }
            $events_json = [
                'id' => $event->getId(),
                'title' => $event->getTitle(),
                'start'=>$event->getStartAtDate()->format('Y-m-d H:i'),
                'end'=>$event->getEndAtDate()->format('Y-m-d H:i'),
                'description'=>$event->getDescription(),
                'location'=>$event->getLocation(),
                'allDay'=>$event->getAllDay(),
                'backgroundColor'=>$event->getBgcolor()??'#33B5E5',
                'shared'=>$event->getShared(),
                'allow_modify'=>$event->getAllowModify(),
                'members'=>$members
            ];
        }
        return $events_json;
    }

    /**
     * @Route("/events/delete/member", name="events-delete-member", methods={"POST"})
     * @param Request $request
     * @param UsersRepository $usersRepository
     * @return Response
     */
    public function delete_member(Request $request,UsersRepository $usersRepository) {

        $event_data = json_decode($request->getContent());
        $message = 'You cannot remove this user as member';
        $status = Response::HTTP_NOT_MODIFIED;
        if ($event_data->event && $event_data->member)
        {
            $event  = $this->eventsRepository->find($event_data->event);
            $member = $usersRepository->find($event_data->member);
            //Only the event's owner or member (only when event is allowed to be modified), can
            // remove member from this event
            if (($event && $member && $this->getUser() == $event->getUsers()) ||
                ($event->getAllowModify() && $event->getMembers()->contains($this->getUser()))
            ) {
                $event->removeMember($member);
                $this->entityManager->flush();
                $message = 'success';
                $status = Response::HTTP_ACCEPTED;
            }
        }
        return new Response($message, $status);
    }

}
