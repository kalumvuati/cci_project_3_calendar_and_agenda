<?php


namespace App\Controller\Users;


use App\Controller\Users\UsersController;
use App\Repository\EventsRepository;
use App\Repository\UsersRepository;
use DateTime;
use DateTimeZone;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{

    /**
     * @Route("/users/dashboard/calendar/search", name="dashboard-search", methods={"POST"})
     * @param UsersRepository $usersRepository
     * @return Response
     */
    public function dashboard_search(UsersRepository $usersRepository) :Response
    {
        //TODO - must find only users who shared calendar
        $users = $usersRepository->findAll();
        $users_json = [];
        foreach ($users as $user) {
            $users_json [] = [
                "id"=>$user->getId(),
                "email"=>$user->getEmail(),
                "name"=>$user->getName(),
                "lastname"=>$user->getLastname()??""
            ];
        }
        return new Response(json_encode($users_json),"200");
    }

}