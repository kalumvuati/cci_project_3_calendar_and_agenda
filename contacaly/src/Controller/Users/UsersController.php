<?php

namespace App\Controller\Users;

use App\Entity\Contacts;
use App\Entity\Events;
use App\Entity\Users;
use App\Form\UsersType;
use App\Helper\EmailSender;
use App\Repository\ContactCategoryRepository;
use App\Repository\ContactsRepository;
use App\Repository\EventsRepository as ER;
use App\Repository\UsersRepository as UR;
use App\Security\EmailVerifier as Email;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface as Em;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface as Session;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface as Tk;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface as En;
use Symfony\Component\Security\Core\Security as Sec;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UsersController extends AbstractController
{
    protected Em $entityManager;
    protected ER $eventsRepository;
    protected Sec $security;
    protected Email $emailVerifier;
    protected UR $usersRepository;
    protected En $passwordEncoder;
    protected tk $tokenStorage;
    protected Session $sessionStorage;

    public function __construct(ER $er, Em $em, Sec $sec, Email $email, UR $users, En $enc, Tk $tk, Session $ses)
    {
        $this->security         = $sec;
        $this->eventsRepository = $er;
        $this->entityManager    = $em;
        $this->usersRepository  = $users;
        $this->emailVerifier    = $email;
        $this->passwordEncoder  = $enc;
        $this->tokenStorage     = $tk;
        $this->sessionStorage   = $ses;
    }

    /**
     * @Route("/users/dashboard", name="users-dashboard", methods={"GET"})
     * @param ContactsRepository $contactsRepository
     * @param ContactCategoryRepository $categoryRepository
     * @return Response
     * @throws \Exception
     */
    public function dashboard(ContactsRepository $contRepo, ContactCategoryRepository $categoryRepository): Response
    {
        /**
         * Search for the current User's events by id
         * @var Events [] $events
         */
        $events         = $this->eventsRepository->findByUsers($this->getUser()->getId());
        $events_json    = [];
        $interval_days  = 3;
        $today          = new DateTime("now",new DateTimezone("Europe/Paris"));
        $nearest_events = $this->eventsRepository->findByUsersCurrent($this->getUser());

        /**
         * Contacts added today
         * @var Contacts [] $contacts
         */
        $contacts       = $contRepo->findAddedToday(20);

        /** @var Events $event */
        foreach ($events as $event)
        {
            //Check how can see events : owner, member, shared event
            if (
                $this->getUser()->getId() === $event->getUsers()->getId() ||
                $event->getShared() ||
                $event->getMembers()->contains($this->getUser())
            )
            {
                //Convert Event PHP Object to Json
                $events_json [] = EventsController::eventToJson($event);
            }
        }

        //Adding event in which current user is member
        $currentUser = $this->usersRepository->find($this->getUser()->getId());

        foreach ($currentUser->getMemberOfEvents() as $likeMember) {
            $likeMember->setBgcolor("#909399");
            $events_json [] = EventsController::eventToJson($likeMember);
        }

        //Statistics areas---------------------------------------------
        $users_nb       = $this->usersRepository->findAll();
        $contacts_nb    = $contRepo->findAll();
        $events_nb      = $this->eventsRepository->findAll();
        $categories_nb  = $categoryRepository->findAll();
        //--------------------------------------------------------------
        return $this->render("users/dashboard.html.twig",
            [
                'nearest_events'=>$nearest_events,
                'events' =>json_encode($events_json),
                'today' =>[
                    "day"=>$today->format('d'),
                    "days"=>EmailSender::DAYS[$today->format('D')],
                    "month"=>EmailSender::MONTHS[$today->format('M')]??$today->format('M'),
                    "year"=>$today->format('Y'),
                ],
                "contacts"=>$contacts,
                "statistic"=> [
                    'users'=>$users_nb,
                    'events'=>$events_nb,
                    'categories'=>$categories_nb,
                    'contacts'=>$contacts_nb
                ]
            ]
        );
    }

    /**
     * @Route("/users/users/{id}/profile", name="users-profile")
     * @param int|null $id
     * @return Response
     */
    public function profile(?int $id): Response
    {
        if (isset($id)) {
            $user = $this->usersRepository->find($id);
        }else {
            $user = $this->getUser();
        }

        return $this->render("users/users/profile.html.twig", [
            'user'=>$user,
            'profile_active'=>'active',
        ]);
    }

    /**
     * @Route("/users/edit/{id}", name="users-edit", methods={"GET","POST"})
     * @param int|null $id
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function edit(?int $id, Request $request, ValidatorInterface $validator): Response
    {
        if (isset($id)) {
            $user = $this->usersRepository->find($id);
        }else{
            /** @var Users $user */
            $user = $this->getUser();
        }

        $message = "Personnalisation du profil utilisateur avec succès!";
        $button_message = "Valider";
        $form_b = $this->createForm(UsersType::class ,$user);
        $form_b->handleRequest($request);

        if ($form_b->isSubmitted() && $form_b->isValid()) {
            $violations = $validator->validate($user);
            $uploadedFile = $form_b['imageFile']->getData();
            if ($uploadedFile) {
                $old_picture_name = $user->getPicture();

                //check if old image exists to delete it
                if (!empty($old_picture_name)) {
                    $old_image_path = $this->getParameter('uploads_base_profile') . "/$old_picture_name";
                    $old_image_path = dirname(__DIR__,3) . (string)$old_image_path;
                    $old_image_path = str_replace("\\", "/", $old_image_path);

                    if (file_exists($old_image_path)) {
                        unlink($old_image_path);
                    }
                }
                $user->setPicture($uploadedFile->getClientOriginalName());
            }
            //Update a date of modification
            $user->setUpdatedAt(new DateTime());
            $this->entityManager->flush();
            $this->addFlash('success', $message);

            return $this->redirectToRoute('users-profile', ['id'=>$user->getId()]);
        }

        $form = $form_b->createView();
        $url = $this->generateUrl("users-profile", ['id'=>$user->getId()]);
        $return_array = compact('user', 'form', 'button_message', 'url');
        return $this->render("users/users/edit.html.twig", $return_array);
    }

    /**
     * @Route("/users-{id}-detele", name="users-delete", methods={"POST", "DELETE"})
     * @Route("/admin/users/detele/{id}", name="admin-users-delete", methods={"POST", "DELETE"})
     * @param int $id
     * @param Request $request
     * @param MailerInterface $mailer
     * @return Response
     * @throws TransportExceptionInterface
     */
    public function delete(int $id, Request $request, MailerInterface $mailer): Response
    {
        $user = $this->usersRepository->find($id);
        if ($request->request->get("role") == "admin") {
            $route = "admin-users-index";
        }
        else {
            $route = "app_login";
            $this->tokenStorage->setToken(null);
        }

        //TODO - there is how to do it auto via Entity annotations
        foreach ($user->getContactCategories() as $cat) {
            $user->removeContactCategory($cat);
        }

        if ($user && $this->isCsrfTokenValid('delete'.$user->getId(), $request->get('_token'))) {

            //Sending mail
            $systeme = new Users();
            $systeme->setName("Service support")
                ->setEmail($this->getParameter("email.messager"));
            EmailSender::sendMail($mailer,$systeme, $user,'Suppression de compte', null, 'unsubscribe');

            $this->entityManager->remove($user);
            $this->entityManager->flush();

            $this->addFlash('success', "Suppression de l'utilisateur avec succès");
        }else {
            $this->addFlash('error', "Erreur lors de suppression de l'utilisateur");
        }

        return $this->redirectToRoute($route);
    }

    /**
     * TODO - to do
     * @Route("/users/messages", name="users-messages")
     * @return Response
     */
    public function message(): Response
    {
        //TODO message page
        return $this->render("users/messages/index.html.twig");
    }

}
