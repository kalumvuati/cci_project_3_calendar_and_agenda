<?php

namespace App\Controller\Users;

use App\Entity\ContactCategory;
use App\Entity\Contacts;
use App\Form\ContactsType;
use App\Repository\ContactCategoryRepository;
use App\Repository\ContactsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactsController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var ContactsRepository $contactsRepository
     */
    private ContactsRepository $contactsRepository;

    /**
     * ContactsController constructor.
     * @param ContactsRepository $repository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ContactsRepository $repository,
        EntityManagerInterface $entityManager)
    {
        $this->contactsRepository   = $repository;
        $this->entityManager        = $entityManager;
    }

    /**
     * @Route("/users/contacts", name="users-contacts")
     * @param Request|null $request : useless when we make a search
     * @param ContactCategoryRepository $contactCategoryRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(
        Request $request = null,
        ContactCategoryRepository $contactCategoryRepository,
        PaginatorInterface $paginator): Response
    {

        $request_array  = $request->query->all();
        $request_array_key_value = [];

        /**
         * Those categories will be used for select tag on front
         * @var ContactCategory $categories
         */
        $categories     = $contactCategoryRepository->findAll();
        foreach ($request_array as $key => $value) {
            if ($value) {
                $request_array_key_value[$key] = $value;
            }
        }

        /**
         * Doing search according options. Ex : Search by name, phone or categories
         */
        if ($request_array_key_value) {
            $contacts = $this->contactsRepository->findByOptions($request_array_key_value);
        } else {
            $contacts = $contacts ?? $this->contactsRepository->findAll();
        }

        $pagination = $paginator->paginate($contacts,$request->query->getInt('page',1),10);

        return $this->render(
            'users/contacts/index.html.twig',
            [
                'pagination' => $pagination,
                'search' => true,
                'desable' => true,
                'contact_active' => 'active',
                'categories' => $categories
            ]
        );
    }

    /**
     * @Route("/users/contacts/{id}/edit", name="users-contacts-edit", methods={"GET|POST"})
     * @Route("/users/contacts/new", name="users-contacts-new", methods={"GET|POST"})
     * @param int|null $id
     * @param Request $request
     * @return Response
     */
    public function edit(?int $id, Request $request): Response
    {
        if (!isset($id)) {
            $button_name= "Créer";
            $message    = "Création de contact avec succès";
            $title      = "Création de nouveau contact";
            $contact    = new Contacts();
            $contact->setUser($this->getUser());
        }else {
            $title      =  "Modification de contact";
            $button_name= "Edit";
            $message    = "Edition de contact avec succès";
            $contact    = $this->contactsRepository->find($id);
        }

        $form   = $this->createForm(ContactsType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->persist($contact);
            $this->entityManager->flush();
            $this->addFlash("success", $message);
            return $this->redirectToRoute('users-contacts');
        }

        return $this->render(
            'users/contacts/add.html.twig',
            [
                'form' => $form->createView(),
                'button_name' => $button_name,
                'title' => $title
            ]
        );
    }

    /**
     * @Route("/users/contacts/delete/{id}", name="users-contacts-delete", methods={"POST", "DELETE"})
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function delete(int $id, Request $request): Response
    {
        $contact = $this->contactsRepository->find($id);
        if ($contact && $this->isCsrfTokenValid("delete" . $contact->getId(), $request->get('_token'))) {
            $this->entityManager->remove($contact);
            $this->entityManager->flush();
            $this->addFlash("success", "Suppression avec succès");
        }
        return $this->redirectToRoute("users-contacts");
    }

    /**
     * @Route("/users/search", name="search")
     * @param Request $request
     * @param ContactCategoryRepository $contactCategoryRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function search(
        Request $request,
        ContactCategoryRepository $contactCategoryRepository,
        PaginatorInterface $paginator): Response
    {
        $request_array = $request->query->all();
        $request_array_key_value = [];
        $categories = $contactCategoryRepository->findAll();

        foreach ($request_array as $key => $value) {
            if ($value) {
                $request_array_key_value[$key] = $value;
            }
        }
        $contacts   = $this->contactsRepository->findByOptions($request_array_key_value);
        $pagination = $paginator->paginate($contacts,$request->query->getInt('page',1),10);

        return $this->render(
            'users/contacts/index.html.twig',
            [
                'pagination' => $pagination,
                'search' => true,
                'desable' => true,
                'categories' => $categories,
            ]
        );
    }
}
