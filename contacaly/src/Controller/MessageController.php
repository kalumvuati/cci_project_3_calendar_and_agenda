<?php


namespace App\Controller;


use App\Entity\Users;
use App\Helper\EmailSender;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
class MessageController extends AbstractController
{

    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @Route("/message/contact", name="message-contact", methods={"POST"})
     * @param Request $request
     */
    public function contact(Request $request): Response
    {

        $contact_data   = json_decode($request->getContent());
        $message        = ['status'=>Response::HTTP_OK, "message"=>"Votre message est envoyé avec succès"];
        $from           = new Users();
        $to             = new Users();

        $from->setEmail($contact_data->email)
            ->setName($contact_data->name);

        $to->setEmail($this->getParameter("email.messager"))
            ->setName("Contacaly - support");

        EmailSender::sendMailContact(
            $this->mailer,
            $from,
            $to,
            $contact_data->subject,
            $contact_data->message
        );


        return new Response(json_encode($message),$message['status']);
    }
}