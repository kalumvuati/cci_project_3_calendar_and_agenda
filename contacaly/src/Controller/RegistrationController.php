<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\RegistrationFormType;
use App\Helper\EmailSender;
use App\Security\EmailVerifier;
use App\Security\UsersAuthenticator;
use App\Repository\UsersRepository;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class RegistrationController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param GuardAuthenticatorHandler $guardHandler
     * @param UsersAuthenticator $authenticator
     * @return Response
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler $guardHandler,
        ClientRegistry $clientRegistry,
        UsersAuthenticator $authenticator): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('users-dashboard');
        }

        $user = new Users();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //Verify password and encode the plain password
            if ($user->getPlainPasswordConf() === $form->get('plainPassword')->getData()) {
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                    (new TemplatedEmail())
                        ->from(new Address($this->getParameter("email.messager"), 'Tutofree - support service'))
                        ->to($user->getEmail())
                        ->subject('Veuillez confirmer votre adresse mail')
                        ->htmlTemplate('registration/confirmation_email.html.twig')
                );

                return $guardHandler->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $authenticator,
                    'main' // firewall name in security.yaml
                );
            }else {
                $this->addFlash('verify_email_error', "Les mots de passe ne correspondent pas");
            }
        }

        $error_js  = false;
        if ($user->getEmail()) {
            $error_js = true;
        }

        //Oauth url redirect for Google
        $google_uri = $clientRegistry->getClient('google_main')->redirect(['email']);

        return $this->render('security/login.html.twig',
            [
            'registrationForm' => $form->createView(),
                'error' => '',
                'social' => true,
                'google_uri' => $google_uri->getTargetUrl(),
                'error_js' => $error_js,
                'last_username' => '',
            ]
        );
    }

    /**
     * @Route("/verify/email", name="app_verify_email")
     * @param Request $request
     * @param UsersRepository $usersRepository
     * @return Response
     */
    public function verifyUserEmail(
        Request $request,
        UsersRepository $usersRepository): Response
    {
        $id = $request->get('id');

        if (null === $id) {
            return $this->redirectToRoute('app_register');
        }

        $user = $usersRepository->find($id);

        if (null === $user) {
            return $this->redirectToRoute('app_register');
        }

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $user);
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());

            return $this->redirectToRoute('app_register');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Votre adresse email a été vérifié');

        return $this->redirectToRoute('users-dashboard');
    }
}
