<?php

namespace App\Entity;

use App\Repository\EventsRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EventsRepository::class)
 */
class Events
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $all_day;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start_at_date;

    /**
     * @Assert\GreaterThanOrEqual(propertyPath="start_at_date")
     * @ORM\Column(type="datetime")
     */
    private $end_at_date;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="events")
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $bgcolor;

    /**
     * @ORM\Column(type="boolean")
     */
    private $shared;

    /**
     * @ORM\Column(type="boolean")
     */
    private $allow_modify;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\ManyToMany(targetEntity=Users::class, inversedBy="member_of_events")
     */
    private $members;

    public function __construct()
    {
        $this->shared = false;
        $this->allow_modify = false;
        $this->created_at = new DateTime("now");
        $this->members = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAllDay(): ?bool
    {
        return $this->all_day;
    }

    public function setAllDay(?bool $all_day): self
    {
        $this->all_day = $all_day;

        return $this;
    }

    public function getStartAtDate(): ?DateTime
    {
        return $this->start_at_date;
    }

    public function setStartAtDate(DateTime $start_at_date): self
    {
        $this->start_at_date = $start_at_date;

        return $this;
    }

    public function getEndAtDate(): ?DateTime
    {
        return $this->end_at_date;
    }

    public function setEndAtDate(DateTime $end_at_date): self
    {
        $this->end_at_date = $end_at_date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getBgcolor(): ?string
    {
        return $this->bgcolor;
    }

    public function setBgcolor(?string $bgcolor): self
    {
        $this->bgcolor = $bgcolor;

        return $this;
    }

    public function getShared(): ?bool
    {
        return $this->shared;
    }

    public function setShared(?bool $shared): self
    {
        $this->shared = $shared;

        return $this;
    }

    public function getAllowModify(): ?bool
    {
        return $this->allow_modify;
    }

    public function setAllowModify(?bool $allow_modify): self
    {
        $this->allow_modify = $allow_modify;

        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTime $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }


    public function setMember(Users $member): bool
    {
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
            return true;
        }
        return false;
    }

    public function addMember(Users $member): self
    {
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
        }

        return $this;
    }

    public function removeMember(Users $member): self
    {
        $this->members->removeElement($member);

        return $this;
    }

    /**
     * @param string $title
     * @param DateTime $start
     * @param DateTime $end
     * @param strinf $description
     * @return $this
     */
    public static  function construct(
        string $title,
        DateTime $start,
        DateTime $end,
        string $description

    ) {
        $event = new Events();
        $event->setTitle($title)
            ->setStartAtDate($start)
            ->setEndAtDate($end)
            ->setDescription($description);
        return $event;
    }
}
