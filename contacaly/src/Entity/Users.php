<?php

namespace App\Entity;

use App\Repository\UsersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\UserInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UsersRepository::class)
 * @UniqueEntity("email", message="Le contenu de ce champ existe déjà.")
 * @Vich\Uploadable
 */
class Users implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    /**
     * @var string The hashed password
     */
    private $plainPasswordConf;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastConnection_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $subscripted_at;

    /**
     * @Assert\Length(min=10, max=14)
     * @Assert\Regex(pattern="/(^[+][^0][0-9]{10}$)|^(00)([^0])?([0-9]{2}||[0-9]{3})([0-9]{9}$)|^(0)[1-9][0-9]{8}$/",
     *     message="Ecrivez un vrai numéro de téléphone")
     * @ORM\Column(type="string", length=14, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $picture;

    /**
     * @Vich\UploadableField(mapping="profile_image", fileNameProperty="picture")
     * @var File | null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $infos;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\OneToMany(targetEntity=Events::class, mappedBy="users", cascade={"remove"})
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $events;

    /**
     * @ORM\ManyToMany(targetEntity=Events::class, mappedBy="members")
     */
    private $member_of_events;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $job_title;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=ContactCategory::class, mappedBy="user")
     */
    private $contactCategories;

    /**
     * @ORM\OneToMany(targetEntity=Contacts::class, mappedBy="user")
     */
    private $contacts;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleSub;

    public function __construct()
    {
        $this->subscripted_at = new \DateTime('now');
        $this->events = new ArrayCollection();
        $this->member_of_events = new ArrayCollection();
        $this->setRoles(['ROLE_USER']);
        $this->contactCategories = new ArrayCollection();
        $this->contacts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getLastConnectionAt(): ?\DateTimeInterface
    {
        return $this->lastConnection_at;
    }

    public function setLastConnectionAt(?\DateTimeInterface $lastConnection_at): self
    {
        $this->lastConnection_at = $lastConnection_at;

        return $this;
    }

    public function getSubscriptedAt(): ?\DateTimeInterface
    {
        return $this->subscripted_at;
    }

    public function setSubscriptedAt(\DateTimeInterface $subscripted_at): self
    {
        $this->subscripted_at = $subscripted_at;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getInfos(): ?string
    {
        return $this->infos;
    }

    public function setInfos(?string $infos): self
    {
        $this->infos = $infos;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection|Events[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Events $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setUsers($this);
        }

        return $this;
    }

    public function removeEvent(Events $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getUsers() === $this) {
                $event->setUsers(null);
            }
        }

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    /**
     * @return Collection|Events[]
     */
    public function getMemberOfEvents(): Collection
    {
        return $this->member_of_events;
    }

    public function addMemberOfEvent(Events $memberOfEvent): self
    {
        if (!$this->member_of_events->contains($memberOfEvent)) {
            $this->member_of_events[] = $memberOfEvent;
            $memberOfEvent->addMember($this);
        }

        return $this;
    }

    public function removeMemberOfEvent(Events $memberOfEvent): self
    {
        if ($this->member_of_events->removeElement($memberOfEvent)) {
            $memberOfEvent->removeMember($this);
        }

        return $this;
    }

    public function getJobTitle(): ?string
    {
        return $this->job_title;
    }

    public function setJobTitle(?string $job_title): self
    {
        $this->job_title = $job_title;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return File|UploadedFile|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File|UploadedFile|null $imageFile
     * @return void
     */
    public function setImageFile(?File $imageFile): self
    {
        $this->imageFile = $imageFile;
        if ($imageFile) {
            $this->updatedAt = new \DateTimeImmutable();
        }
        return $this;
    }

    public function serialize()
    {
        return serialize([$this->id, $this->email, $this->password]);
    }

    public function unserialize($serialized)
    {
	    list($this->id, $this->email, $this->password) = unserialize($serialized, ['allowed_classes'=>false]);
    }

    /**
     * @return Collection|ContactCategory[]
     */
    public function getContactCategories(): Collection
    {
        return $this->contactCategories;
    }

    public function addContactCategory(ContactCategory $contactCategory): self
    {
        if (!$this->contactCategories->contains($contactCategory)) {
            $this->contactCategories[] = $contactCategory;
            $contactCategory->setUser($this);
        }

        return $this;
    }

    public function removeContactCategory(ContactCategory $contactCategory): self
    {
        if ($this->contactCategories->removeElement($contactCategory)) {
            // set the owning side to null (unless already changed)
            if ($contactCategory->getUser() === $this) {
                $contactCategory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contacts[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contacts $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setUser($this);
        }

        return $this;
    }

    public function removeContact(Contacts $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getUser() === $this) {
                $contact->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainPasswordConf(): ?string
    {
        return $this->plainPasswordConf;
    }

    /**
     * @param string|null $plainPasswordConf
     * @return Users
     */
    public function setPlainPasswordConf(?string $plainPasswordConf): Users
    {
        $this->plainPasswordConf = $plainPasswordConf;
        return $this;
    }

    public function getGoogleSub(): ?string
    {
        return $this->googleSub;
    }

    public function setGoogleSub(?string $googleSub): self
    {
        $this->googleSub = $googleSub;

        return $this;
    }
}
