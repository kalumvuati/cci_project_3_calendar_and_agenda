create table doctrine_migration_versions
(
    version        varchar(191) not null
        primary key,
    executed_at    datetime     null,
    execution_time int          null
)
    collate = utf8_unicode_ci;

create table users
(
    id                 int auto_increment
        primary key,
    email              varchar(180) not null,
    roles              longtext     not null comment '(DC2Type:json)',
    password           varchar(255) null,
    name               varchar(255) not null,
    lastname           varchar(100) null,
    last_connection_at datetime     null,
    subscripted_at     datetime     not null,
    phone              varchar(50)  null,
    picture            varchar(255) null,
    infos              longtext     null,
    is_verified        tinyint(1)   not null,
    job_title          varchar(50)  null,
    updated_at         datetime     null,
    google_sub         varchar(255) null,
    facebook_id        varchar(255) null,
    github_id          varchar(255) null,
    constraint UNIQ_1483A5E9E7927C74
        unique (email)
)
    collate = utf8mb4_unicode_ci;

create table contact_category
(
    id       int auto_increment
        primary key,
    user_id  int          null,
    category varchar(100) not null,
    constraint FK_9C698556A76ED395
        foreign key (user_id) references users (id)
)
    collate = utf8mb4_unicode_ci;

create index IDX_9C698556A76ED395
    on contact_category (user_id);

create table contacts
(
    id          int auto_increment
        primary key,
    category_id int          null,
    user_id     int          null,
    names       varchar(100) not null,
    email       varchar(255) null,
    phone       varchar(50)  not null,
    picture     varchar(255) null,
    site        varchar(100) null,
    address     varchar(100) null,
    infos       longtext     null,
    jobtitle    varchar(50)  null,
    created_at  datetime     not null,
    constraint UNIQ_33401573444F97DD
        unique (phone),
    constraint UNIQ_33401573E7927C74
        unique (email),
    constraint FK_3340157312469DE2
        foreign key (category_id) references contact_category (id),
    constraint FK_33401573A76ED395
        foreign key (user_id) references users (id)
            on delete set null
)
    collate = utf8mb4_unicode_ci;

create index IDX_3340157312469DE2
    on contacts (category_id);

create index IDX_33401573A76ED395
    on contacts (user_id);

create table events
(
    id            int auto_increment
        primary key,
    users_id      int          null,
    title         varchar(255) not null,
    all_day       tinyint(1)   null,
    start_at_date datetime     not null,
    end_at_date   datetime     not null,
    description   longtext     not null,
    picture       varchar(255) null,
    location      varchar(255) null,
    bgcolor       varchar(10)  null,
    shared        tinyint(1)   not null,
    allow_modify  tinyint(1)   not null,
    created_at    datetime     not null,
    constraint FK_5387574A67B3B43D
        foreign key (users_id) references users (id)
)
    collate = utf8mb4_unicode_ci;

create index IDX_5387574A67B3B43D
    on events (users_id);

create table events_users
(
    events_id int not null,
    users_id  int not null,
    primary key (events_id, users_id),
    constraint FK_A43F6DCF67B3B43D
        foreign key (users_id) references users (id)
            on delete cascade,
    constraint FK_A43F6DCF9D6A1065
        foreign key (events_id) references events (id)
            on delete cascade
)
    collate = utf8mb4_unicode_ci;

create index IDX_A43F6DCF67B3B43D
    on events_users (users_id);

create index IDX_A43F6DCF9D6A1065
    on events_users (events_id);

create table reset_password_request
(
    id           int auto_increment
        primary key,
    user_id      int          not null,
    selector     varchar(20)  not null,
    hashed_token varchar(100) not null,
    requested_at datetime     not null comment '(DC2Type:datetime_immutable)',
    expires_at   datetime     not null comment '(DC2Type:datetime_immutable)',
    constraint FK_7CE748AA76ED395
        foreign key (user_id) references users (id)
)
    collate = utf8mb4_unicode_ci;

create index IDX_7CE748AA76ED395
    on reset_password_request (user_id);


