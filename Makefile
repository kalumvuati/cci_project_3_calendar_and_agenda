.PHONY: install clean archive

#Documentations--------------------------------------------
CODE = contacaly docs *.md
NAME = "KALUMVUATI_Duramana"

install:
	cd contacaly;composer install;yarn install;yarn dev; php bin/console doctrine:database:create;php bin/console doctrine:migrations:migrate;php bin/console doctrine:fixtures:load; symfony serve -d

# Deletting all files that can be reproduced
clean :
	cd contacaly; php bin/console cache:clear; rm -rf vendor node_modules ./idea

# Archive
archive: $(CODE)
	tar -czf ${NAME}.tar.gz --transform="s,^,${NAME}/,"	$(CODE) Makefile

