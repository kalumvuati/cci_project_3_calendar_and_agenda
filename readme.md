## PROJET N° 3 - CONTACALY
___

Fait par __Duramana KALUMVUATI__

### 1. Objetif :

J'ai mis en  votre disposition le fichier contenant le sujet de ce projet.

### 2. Liste de technologies utilisées dans ce projet:

- HTML5
- CSS3 (SASS)
- Javascript (JQuery)
- PHP (Symfony Framework)
- MySQL


### 3. INSTALLATIONS

J'ai aussi utilisé :

* [FullCalendar](https://fullcalendar.io/docs) : affichage de calendrier
* [Select2](https://select2.org/) : Permet le multi selection des options dans un select.
* Et autres bundle symfony qui se trouvent bien-sûr dans le fichier _composer.json_

Pour la gestion de version et la suivie, j'ai utilisé :

- git (en local)
- gitlab (en remote) [depôt](https://gitlab.com/kalumvuati/cci_project_3_calendar_and_agenda)
- trello (en remote) [lien](https://trello.com/b/rapfYgg7/cciprojet3calendrieretcarnet-l)

### 3.1. Windows

1. Installer le serveur Wampp :
>https://www.wampserver.com/#wampserver-64-bits-php-5-6-25-php-7

2. Installer  _composer_ :
>https://getcomposer.org/download/ ou https://getcomposer.org/Composer-Setup.exe.

3. Installer _symfony_ :
>https://symfony.com/download

5. Installer _[nodejs](https://nodejs.org/en/)_ : on y trouve _npm_ par lequel vous pouvez installer _yarn_
 ```shell
npm install --global yarn
```
6. Installer _git_ :
>https://git-scm.com/downloads

7. Cloner le depot en ligne en utilisant la commande (BASH) : (ou faîtes le download directement)

```shell 
git clone https://gitlab.com/kalumvuati/cci_project_3_calendar_and_agenda.git
```

Vous aurez un repertoire nommé _"cci_project_3_calendar_and_agenda"_.
Et lancez votre terminal _GITBASH_ ou _Powelshell_, et suivez ces étapes
```shell
    # si vous le depot est sur le dossier personnel. soit, rendez-vous où se trouve le dossier projet
cd; cd cci_project_3_calendar_and_agenda/contacaly 
composer install 🎼
yarn install  🎼 # ou npm install
yarn dev 🎼 # dev ou
yarn build 🎼 #  prod
```
8. Configuration du fichier _.env_, il faut personnaliser les variables d'environnement selon la configuration de la base de données.
Pour faire simple :
```shell
APP_ENV=dev
APP_SECRET=
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name"
    # DATABASE_URL="postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=13&charset=utf8"
MAILER_DSN=null://localhost

    ###> symfony/swiftmailer-bundle ###
    # For Gmail as a transport, use: "gmail://username:password@localhost"
MAILER_URL=null://localhost
    ###< symfony/swiftmailer-bundle ###
```

9. Assurez-vous que le server _Wampp soit lancé, dans ce cas faites ceci: 
```shell
    # création de la base de données
php bin/console doctrine:database:create
    # Création de tables 
php bin/console doctrine:migrations:migrate 
    #Mise en place les fausses données
php bin/console doctrine:fixtures:load 
    #Lancer le seveur 
symfony server:start -d 
```

Vous aurez sur l'écran ce message :

```shell
 [WARNING] run "symfony server:ca:install" first if you want to run th  
 e web server with TLS support, or use "--no-tls" to avoid t            
 his warning                                                            
                                                                        
                                                                        
 [OK] Web server listening                                              
      The Web server is using PHP CLI 7.4.3                             
      http://127.0.0.1:8000 
```

10. Vous n'avez qu'à utiliser ce lien sur votre navigateur
11. Enjoy ✔️  [lien](http://contacaly.tutofree.fr/)

### 3.2. Linux ou MacOs

Vous avez la chance ! Je vous ai mis en place un fichier Makefile qui vous permettra à faire 
des choses automatiquement.
```shell
    # Installation de git 
sudo apt install git
    # installation de composer
sudo apt install composer
    # Installation de node : https://tecadmin.net/install-nodejs-with-nvm/
sudo apt install node
    # installation de yarn 
npm install --global yarn
    # installation de xampp télécharger sur https://www.apachefriends.org/download.html 
sudo chmod u+x  xampp-linux-x64-8.0.3-0-installer.run
sudo ./ xampp-linux-x64-8.0.3-0-installer.run
    #Lancer 
sudo /opt/lampp/xampp start&
    # Installation de symfony 
wget https://get.symfony.com/cli/installer -O - | bash
    # telecharger le projet à partir du depot git
git clone https://gitlab.com/kalumvuati/cci_project_3_calendar_and_agenda.git
    # Rentre rendre dans le dossier du projet
cd cci_project_3_calendar_and_agenda
    # Fait le reste. Vous pouvez consulter ce fichier pour voir le contenu, pas grande chose.& cat Makefile
make install
```

Eh, voilà.

NB : si vous avez de problème d'installation ou autres, n'hésitez surtout pas de nous écrire ✏️  sur ce mail :
✉️ duramana.kalumvuati@laposte.net.

Nous vous remercions.

### 5. DIFFICULTES 

1. vu que j'avais mise en place sur la même page le formulaire de _login_ et _s'inscrire_. 
   Lors d'inscription, une erreur de token CSRF empêchait de retrouver la route pour s'inscrire. Vu que ces deux dernières ciblaient le même url.
   
__Solution__ : forcer la route en rajoutant la bâlise _form_ ci-dessous 
```twig 
    <form action="{{ path('app_register') }}" methode="post">
    
    </form>
```
Après, j'ai dû faire la gestion de routes côté contrôleur, etc.

2. Installation de FullCalendar en utilisant Webpack en Symfony : 

_Problème_ : j'avais importé le paquet, après son installation en utilisant la commande suivante :
```sh
    yarn add fullcalendar
```
Je n'arrivais pas à l'importer dans le fichier _assets/app.js_ pour pouvoir l'utiliser dans un autre fichier
spécialement dédié à fullCalendar.

J'avais une erreur qui disait que la classe FullCalendar n'était pas définie. En fait, elle était chargée après la vue.

__Solution__ : 
Je me suis lancé dans la documentation pour savoir comment importer le paquet FullCalendar en utilisant _Webpack_.
>https://fullcalendar.io/docs/initialize-es6

j'ai inclus directement le script sur le fichier approprié, puis j'ai fait ceci.
```twig
{% block javascripts %}
<script src="..."></script>
    {{ parent() }}
    ...
{% endblock %}
```

3. Lors que je voulais utiliser mon PC sur lequel j'ai installé Linux. Juste après avoir cloné le depot git, et configuré le fichier
.env. Rien ne marchais et j'avais une erreur de genre [SQL...] 2002.... En fait, j'avais oublié de changer le port pour avoir accès à ma base de données.

__Solution__ : changé le port de 5432 à 3306, et voilà.

4. Travailler avec des dates de javascript à PHP, c'est toujours compliqué. Lors que je voulais obtenir la date dont le 
   jour ou mois était inférieure à 9. 

__Solution__ : J'avais mis en place une fonction qui rajoute 0 devant la valeur inférière à 10 pour qu'il y ait deux caractères. Sinon, son affichage côté html ne marchait pas dans la bâlise input:datetime. 
   Vu que le constructeur d'une date ou heure est la suivante ('yyyy-mm-dd' 'hh:mm:ss'). 
   En plus de ce là, la date fournie par l'objet _Event_ de fullCalendar me rajoute 1 heure supplémentaire, il a fallu tout traiter.

5. Comment prendre en compte les modifications d'un événement sur le fullCalendar puis actualiser le calendrier sans avoir chargé la page ?.

__Solution__ : Plusieurs obstacles, mais j'ai réussi à m'adapter en supprimant ce dernier affiché sur le calendrier, puis le réafficher avec les nouvelles valeurs. 

6. Problème sur la suppression en cascade côté base de données. J'avais fait la relation ManyToOne dans la mauvaise entité. Donc, lors de suppression d'un contact, la catégorie qu'était utilisée par ce contact était supprimée, puis les autres contacts ayant cette catégorie se supprimaient aussi(CASCADE).

__Solution__ : Donc, j'ai dû refaire mes relations et tout rentré dans l'ordre.
7. Problème de Serialisation lors d'utilisation de la Classe/Interface File dans la classe User. Cette erreur est apparue lors que je voulais include upload de photo (Vich...). 
   Puisque tout marchait bien. Et j'avais une erreur qui disait que : "impossible de serialisation du fichier File"
   
Solution : 
```php
    public function serialize()
    {
    	// TODO: Implement serialize() method.
        return serialize([$this->id,$this->email,$this->password]);
    }

    public function unserialize($serialized)
    {
	// TODO: Implement unserialize() method.
	list($this->id,$this->email,$this->password
                           ) = unserialize($serialized,['allowed_classes'=>false]);
    }
```

